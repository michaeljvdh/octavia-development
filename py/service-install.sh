#!/bin/bash
clear 
echo ""
echo ""
echo "SERVICE INSTALLS"
echo ""

echo "Creating Links ..."
echo ""
cp  *.service /etc/systemd/system
sudo systemctl enable dhtfetch
sudo systemctl enable ldd
sudo systemctl enable rules
sudo systemctl enable rulesact
sudo systemctl enable schedgpio
sudo systemctl enable schedesp
sudo systemctl enable telegram
sudo systemctl enable therm

echo PLEASE REBOOT YOUR PI NOW TO ACTIVATE THESE SERVICES.
echo Once your Pi has booted up please check the services tab on the website,
echo the help icon on the services page will indicate what you should or should not see.

