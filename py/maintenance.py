debug=0
import time
import os
import dbconnection
import MySQLdb
import json
import shutil
send=1

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()

checkpoint = time.time()
x=1
while x==1:	
	try:
		f = open("mynode.txt", "r")
		nodeselected = f.read()
		nodeselected = nodeselected.replace("\n","")
		nodeselected = nodeselected.replace("`","")
		f.close()
		
		total, used, free = shutil.disk_usage("/")
		# print("Total: %d GiB" % (total // (2**30)))
		# print("Used: %d GiB" % (used // (2**30)))
		# print("Free: %d GiB" % (free // (2**30)))
		freespace = ("%d" % (free // (2**30)))
		freespace = int(freespace)
		if freespace < 1:	
			if send == 1:		
				curs.execute('INSERT INTO messages SET message="<strong>Disk Space Below 1GB</strong>.", node="%s";' %(nodeselected))
				db.commit()
				send = 0
			else:
				send = 1			

		curs = db.cursor()

		curs.execute ('SELECT * FROM config WHERE description="log" AND set1 = "global";')
		db.commit()
		results = curs.fetchall()
		for row in results:
			display = row[5]
			history = row[4]

		counta = 0
		countb = 0
		countc = 0

		uniqueTableIds = []
		curs.execute ('SELECT * FROM log GROUP BY table_id, attr;')
		db.commit()
		results = curs.fetchall()
		for row in results:
			table_id = row[2]
			attr = row[3]
			counta = counta +1

			exclusion = []
			curs.execute ('SELECT * FROM log WHERE table_id=%s AND attr="%s" ORDER BY id DESC LIMIT %s;'%(table_id,attr,history))
			db.commit()
			resultsa = curs.fetchall()
			for rowa in resultsa:
				ida = rowa[0]
				countb = countb +1
				exclusion.append(ida)
			countb = 0
			exclusion = ','.join(map(str,exclusion))

			countc = 0
			curs.execute ('DELETE FROM log WHERE table_id=%s AND attr="%s" AND id NOT IN (%s);'%(table_id,attr,exclusion)) #Becomes DELETE FROM
			db.commit()
			resultsb = curs.fetchall()
			for rowb in resultsb:
				idb = rowb[0]
				countc = countc +1
			countc=0		
		time.sleep(5)

	except Exception as e:
		if debug==1:
			print(e)