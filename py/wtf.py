debug=0

import MySQLdb
import time
import dbconnection
import os

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)

curs = db.cursor()
curs.execute ("SELECT id FROM log WHERE `table`='ds18b20' ORDER BY id DESC LIMIT 10")
results = curs.fetchall()
db.commit()
keep = []
for row in results:	
	keep.append(str(row[0]))
keepers = ",".join(keep)
print (keepers)
curs.execute ("DELETE FROM log WHERE `table`='ds18b20' AND id NOT IN (%s)"%(keepers))
db.commit()

