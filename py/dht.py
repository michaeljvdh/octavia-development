debug=0

import time
import adafruit_dht
import sys
import dbconnection
import MySQLdb


dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)

gpio=sys.argv[1]
dolog=sys.argv[2]
id_id=sys.argv[3]

dolog = int(dolog)
id_id = int(id_id)

gpio = int(gpio)

if gpio == 3:    pin = 2 
if gpio == 5:    pin = 3
if gpio == 7:    pin = 4
if gpio == 8:    pin = 14
if gpio == 10:    pin = 15
if gpio == 11:    pin = 17
if gpio == 12:    pin = 18
if gpio == 13:    pin = 27
if gpio == 15:    pin = 22
if gpio == 16:    pin = 23
if gpio == 18:    pin = 24
if gpio == 19:    pin = 10
if gpio == 21:    pin = 9
if gpio == 22:    pin = 25
if gpio == 23:    pin = 11
if gpio == 24:    pin = 8
if gpio == 26:    pin = 7
if gpio == 29:    pin = 5
if gpio == 31:    pin = 6
if gpio == 32:    pin = 12
if gpio == 33:    pin = 13
if gpio == 35:    pin = 19
if gpio == 36:    pin = 16
if gpio == 37:    pin = 26
if gpio == 38:    pin = 20
if gpio == 40:    pin = 21

# Initial the dht device, with data pin connected to:
dhtDevice = adafruit_dht.DHT22(pin)

try:
    f = open("mynode.txt", "r")
    nodeselected = f.read()
    nodeselected = nodeselected.replace('`','')
    nodeselected = nodeselected.replace('\n','')
    f.close()
    nodeselected = str(nodeselected)

    humidity = dhtDevice.humidity
    temperature = dhtDevice.temperature
    if debug==1:
        print (humidity)    
        print (temperature)    
    datapull= humidity    
    f = open("/mnt/octavia/%s.dat" % (gpio), "w" )
    f.write(str(datapull))
    f.close()
    f = open("/mnt/octavia/%st.dat" % (gpio), "w" )
    f.write(str(temperature))
    f.close()

    if dolog==1:        
        if debug==1:
            print("write to log")
        curs = db.cursor()
        curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value) VALUES ('dht1122','%s','temperature','%s','%s');" % (id_id,nodeselected,float(temperature)))
        db.commit()
        curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value) VALUES ('dht1122','%s','humidity','%s','%s');" % (id_id,nodeselected,float(humidity)))
        db.commit()

except Exception as e:
    if debug==1:
        print(e)