debug=0

import time
import dbconnection
# import sys
import os
import MySQLdb
# import subprocess
from datetime import datetime
# from multiprocessing import Process
import RPi.GPIO as GPIO
# import json
 
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()


runloop=1
count = 0

while runloop ==1: 
    try:
        time.sleep(1)
        try:
            f = open("mynode.txt", "r")
            nodeselected = f.read()
            nodeselected = nodeselected.replace("\n","")
            nodeselected = nodeselected.replace("`","")
            f.close()
    
        except:
            if debug == 1:
                print ("Failed to read mynode.txt")
            else:
                donothing = 1
                
        curs.execute ('SELECT * FROM iffy3 WHERE node="%s";' % (nodeselected))
        results = curs.fetchall() 
        db.commit()       
        if (results==()):
            if debug==1:
                print ("Noresults")
                time.sleep(0.2)
        else:
            for row in results:           
                id = row[0]
                iffy3id = row[0]
                alert=row[8]
                revert = row[11]
                action = row[10]
                iff3description = row[7]            
                alertcan=row[9]
                enable = row[5]
                objectname=row[3]
                objectname_csv=objectname.split(",")
                objecttype=objectname_csv[0]
                objecttype_number=objectname_csv[1]                
                objectvalue = row[4]            
                objectvalue = (int(objectvalue))
                if count ==0 and enable==1:
                    curs.execute ('UPDATE pca9685 set run=1 WHERE node = "%s"'%(nodeselected))
                    count=1
                fact = row[2]
                
                csv=objectname.split(",")
                sensor = csv[0]
                sensorid = csv[1]
                iffy2_id=row[1]
                validator_id=row[12]       
                # print ("START",objectvalue)    
                if iffy2_id==0:
                    curs.execute ('SELECT * FROM validator WHERE id="%s" AND node = "%s"'%(validator_id,nodeselected))
                    resultsB = curs.fetchall()
                    for rowB in resultsB:
                        c = rowB[8]
                        c = int(c)
                        testResult = c
                curs.execute ('SELECT * FROM iffy2 WHERE id="%s" AND node = "%s"'%(iffy2_id,nodeselected))
                resultsB = curs.fetchall()
                for rowB in resultsB:
                    objectname=rowB[6]
                    csv=objectname.split(",")
                    sensortested= csv[0]
                    sensortestedid= csv[1]
                    a = rowB[3]
                    c = rowB[5]
                    c = int(c)
                    testResult = c
                db.commit()               
                if objectname=='gpio,0':
                    skip="yes"
                else:
                    if sensortested=="ds18b20":
                        curs.execute ('SELECT * FROM ds18b20 WHERE id="%s" AND node = "%s"'%(sensortestedid,nodeselected))
                        resultsB = curs.fetchall()
                        for rowB in resultsB:
                            serialnumber = rowB[2]
                        f = open("/mnt/octavia/%s.dat" % (serialnumber), "r" )
                        datavalue=f.read()
                        datavalue = "based on a therm reading changing to <strong>%s</strong>" % datavalue           
                        f.close()
                        db.commit()
                    if sensortested=="dht1122":
                        curs.execute ('SELECT * FROM dht1122 WHERE id="%s" AND node = "%s"'%(sensortestedid,nodeselected))
                        resultsB = curs.fetchall()
                        for rowB in resultsB:
                            serialnumber = rowB[2]
                        f = open("/mnt/octavia/%s.dat" % (serialnumber), "r" )
                        datavalue=f.read()
                        datavalue = "based on a dht reading changing to <strong>%s</strong>" % datavalue
                        f.close()
                        db.commit()
                    if sensortested=="gpio":
                        curs.execute ('SELECT * FROM gpio WHERE id="%s" AND node="%s"'%(sensortestedid,nodeselected))
                        resultsB = curs.fetchall()
                        for rowB in resultsB:
                            gpiodescription=rowB[1]
                        datavalue="based on a gpio <strong>%s</strong> changing to <strong>%s</strong>" % (gpiodescription,a)
                        db.commit()
                    if sensortested=="esp":
                        curs.execute ('SELECT * FROM esp WHERE id="%s" AND node="%s"'%(sensortestedid,nodeselected))
                        resultsB = curs.fetchall()
                        for rowB in resultsB:
                            gpiodescription=rowB[1]
                        datavalue="based on a espgpio <strong>%s</strong> changing to <strong>%s</strong>" % (gpiodescription,a)
                        db.commit()
        
                # print (sensortested,datavalue)
                
        
                if enable == 1:  #STEP 1 Rules is enabled proceed to execute successive steps                    
                    if debug==1:
                        print (objectname, "<----")
                    if sensor=='telegram':                        
                        typeofsensor = 'Telegram'
                        sensorname = "Message"
                        skip="yes"
                    if objectname=='gpio,0': #No GPio has been set, new entry, skip until set
                        skip="yes"
                    else: #Continuation for STEP 1
                        if debug==1:
                                print ("Processing GPIO", iff3description)
                        
                        #Sensor Target Check -------------------------  START

                        if sensor == ("gpio"):
                            typeofsensor = "GPIO"
                            curs.execute ('SELECT * FROM gpio WHERE id="%s" AND node="%s"'%(sensorid,nodeselected))
                            db.commit()
                            results = curs.fetchall()
                            for row in results:
                                sensorname=row[1]
                                gpio=row[2]
                                polarity=row[3]                    
                            # print ("object value",objectvalue)
                            # if (int(polarity)) == 0:
                                # print ("Polarity is 1")                                              
                                    # print ("obj2")
                            # print ("PRE",objectvalue)
                            if (int(polarity)) == 0:
                                if int(objectvalue)==1:
                                    # print ("A")
                                    objectvalue=0
                                else:
                                    # print ("B")
                                    objectvalue=1
                            # print (objectvalue)
                            # exit()
                            # print (polarity,"objvalue=",objectvalue)
                            if fact == testResult:
                                if debug==1:
                                    print ("Validator = Initiator for GPIO:",gpio)
                                GPIO.setup(int(gpio), GPIO.OUT)
                                GPIO.output(int(gpio), objectvalue)
                            if fact != testResult:
                                if debug==1:
                                    print ("Reverting from gpio state:",objectvalue)
                                if revert==1:
                                    if objectvalue==1:
                                        objectvalue=0
                                    if objectvalue==0:
                                        objectvalue==1
                                    GPIO.setup(int(gpio), GPIO.OUT)
                                    GPIO.output(int(gpio), objectvalue)

                                # print ("Firing",objectvalue)
                            
                        if sensor == ("esp"):
                            typeofsensor = "ESP"
                            curs.execute ('SELECT * FROM esp WHERE id="%s" AND node="%s"'%(sensorid,nodeselected))                        
                            results = curs.fetchall()
                            db.commit()
                            for row in results:
                                sensorname=row[1]
                                gpio=row[2]
                                ipaddr=row[4]
                                polarity=row[3]
                                on="on"
                                off="off"
                            if polarity == 0:
                                if objectvalue == 0:
                                    on = "off"
                                if objectvalue == 1:
                                    off = "on"
                            if fact == testResult:
                                if objectvalue==1:
                                    statement = "curl --silent --output --url 'http://192.168.0.23/gpio/d%s%s' >/dev/null 2>&1" %(gpio,on)
                                if objectvalue==0:
                                    statement = "curl --silent --output --url 'http://192.168.0.23/gpio/d%s%s' >/dev/null 2>&1" %(gpio,off)
                                x = os.system(statement)
        
                        if sensor == ("pca9685"):
                            typeofsensor = "PCA9685"
                            # print (typeofsensor,fact,testResult)
                            if int(fact) == int(testResult):
                                
                                curs.execute ('SELECT * FROM pca9685 where id="%s" and node="%s"' % (sensorid,nodeselected))
                                db.commit()
                                results_pca9685 = curs.fetchall()
                                for row_pca9685 in results_pca9685:
                                    sensorname=row_pca9685[1]
                                    name = row_pca9685[1]
                                    current_range=row_pca9685[3]
                                    devicetype = row_pca9685[6]
                                    lastrange = row_pca9685[4]
                                    if devicetype == 0:
                                        pca9685_value_new = objectvalue * 560 / 100 + 90
                                        pca9685_value_new = int(pca9685_value_new)
                                    if devicetype == 1:
                                        pca9685_value_new = objectvalue * 4095 / 100
                                        pca9685_value_new = int(pca9685_value_new)

                                    # print (objectvalue, pca9685_value_new,"--",current_range)         
                                    if pca9685_value_new != current_range:
                                        ignoreme = 1
                                        # print ("herehere")
                                        curs.execute ('UPDATE pca9685 SET `range`="%s", lastrange="%s", run=1 WHERE id="%s" and node="%s"' % (pca9685_value_new,current_range,sensorid,nodeselected))
                                        db.commit()                                    
                                    else:
                                        donothing=1
                        

                        #Sensor Target Check -------------------------  END

                        #Below is just for status and messaging.
                        if int(fact) == int(testResult): #Meaning that the validator and initiator expectation is equal.
                            if action==0: #This path changes action to 1, and based on it being 1 is constantly executes that command.
                                # print ("Send Message")
                                curs.execute ('UPDATE iffy3 SET action="1" WHERE id="%s" AND node="%s"' % (id,nodeselected))
                                if objectvalue==1:
                                    objectvaluedo="On"
                                else:
                                    objectvaluedo="Off"
                                if typeofsensor=="PCA9685":                                    
                                    objectvaluedo = "%s percent" % objectvalue
                                if debug==1:
                                    print (objectvaluedo)
                                    print (typeofsensor)
        
        
                                details = ("Rule <strong>%s</strong> - <strong>%s</strong> - <strong>%s</strong> set to <strong>%s</strong> %s" % (iff3description,typeofsensor,sensorname,objectvaluedo,datavalue))                   
                                if sensor == "telegram":
                                    details = ("Rule <strong>%s</strong> - <strong>%s</strong> - <strong>%s</strong> sent %s - No action taken." % (iff3description,typeofsensor,sensorname,datavalue))

                                # print (details)
                                if alert==1:
                                    curs.execute ('INSERT INTO messages SET message="%s", node="%s",iffy3_id="%s";' %(details,nodeselected,iffy3id))
                                    db.commit()
                                
                                # else:
                                    # skip=1
                        
                        if int(fact) != int(testResult):
                            if action==1:
                                # print ("THERE")
                                curs.execute ('UPDATE iffy3 SET action="0" WHERE id="%s" AND node="%s"' % (id,nodeselected))
                                db.commit()
            
                            
                        # curs.execute ('SELECT * FROM iffy2 WHERE id="%s"'%(iffy2_id))
                        # resultsC = curs.fetchall()
                        # for rowC in resultsC:                    
                        #     description = rowC[1]
                        #     # print (description)                    
                        #     ruletype = rowC[2]
                        #     if ruletype == 0:
                        #         ruledescription =("Between Values")
                        #     if ruletype == 1:
                        #         ruledescription =("Between Time")
                        #     if ruletype == 2:
                        #         ruledescription ="Is Equal To"
                        #     if ruletype == 3:
                        #         ruledescription ="GPIO is"
                        #     a = rowC[3]
                        #     b = rowC[4]                    
            
                        #     db.commit()
                        #     parta = "<b>A Rule Trigerred</b>"            
                        # if debug ==1:
                            # print (objectvalue)
                            # if objectvalue==1:
                                # sent="On"
                            # else:
                                # sent="Off"
                            # print ("Test Val Rule:",iffy2_id,"Sensor Type: ",sensor,"ACT fact:",fact,"TestResult:",testResult,"polarity",polarity,"value sent",sent)
                            
                        # if testResult!=fact:
                            # curs.execute('DELETE FROM messages WHERE iffy3_id="%s";' %(iffy3id))
                            # db.commit()              
                        else:
                            donothing=1
                        # time.sleep(0.5)
    except Exception as e:
        if debug==1:                
            print(e)
    
