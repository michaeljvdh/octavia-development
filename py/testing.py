debug=0
import time
import os
import dbconnection
import MySQLdb
import json
import shutil

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()

f = open("mynode.txt", "r")
nodeselected = f.read()
nodeselected = nodeselected.replace("\n","")
nodeselected = nodeselected.replace("`","")
f.close()


curs.execute ("SELECT * FROM config where node='%s' and description='log' and set1='dht1122';" % (nodeselected))
db.commit()	
results = curs.fetchall()
for row in results:
	dht1122 = row[4]	

curs.execute ("SELECT * FROM config where node='%s' and description='log' and set1='ds18b20';" % (nodeselected))
db.commit()	
results = curs.fetchall()
for row in results:
	ds18b20 = row[4]

curs.execute ("SELECT * FROM config where node='%s' and description='log' and set1='bme';" % (nodeselected))
db.commit()	
results = curs.fetchall()
for row in results:
	bme = row[4]


curs.execute ("SELECT * FROM log where node='%s' GROUP BY table_id, attr;" % (nodeselected))
db.commit()	
results = curs.fetchall()
for row in results:
	table_id = row[2]
	attr = row[3]
	curs.execute ("SELECT * FROM log where table_id='%s' and attr='%s' and node='%s' ORDER by id DESC;" % (table_id,attr,nodeselected))
	db.commit()
