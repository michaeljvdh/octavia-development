import MySQLdb
import datetime
import RPi.GPIO as GPIO
import subprocess
import dbconnection
import time
from prettytable import PrettyTable
import os
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)


# table.field_names = ["Description","State","gpio","action","y","hold","begin","now","end","esp"]
# table.add_row(["City name", "Area", "Population", "Annual Rainfall","s","s","s","s","s","s"])
# # table.add_row(["Adelaide", 1295, 1158259, 600.5])

# print (table)

table = PrettyTable()
table.field_names = ["description","ON","gpio","Act","P","hold","begin","now","end","esp","SKIP"]

tableiffy = PrettyTable()
tableiffy.field_names = ["Rulename","2","Type & ID","Val","5","6","GPIO","Relay Name","9","node","enable","Decision"]

debug = 1

def relayon(x,y,skip):
	if actiondb==0:		
		curs = db.cursor()
		if debug==1:
			print ("Write to SQL")
		curs.execute ('UPDATE octavia.%s SET action="1" WHERE id=%s' % (nodeselected,idd))
		db.commit()
	action = 0
	if y == 1:
		action = 1
	table.add_row ([description,bcolors.OKGREEN +"ON "+ bcolors.ENDC,gpio,action,y,hold,begin,now,end,esp,skip])
	try:
		if action==1:
			statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
			os.system (statement)
		else:
			statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)
	except Exception as e:
		if debug ==1:
			print(e)
	
def relayoff(x,y,skip):	
	if actiondb==1:
		curs = db.cursor()
		if debug==1:
			print ("Write to SQL")
		curs.execute ('UPDATE octavia.%s SET action="0" WHERE id=%s' % (nodeselected,idd))
		db.commit()
	action = 1
	if y == 1:
		action = 0
	table.add_row ([description,bcolors.FAIL + "OFF" + bcolors.ENDC,gpio,action,y,hold,begin,now,end,esp,skip])
	try:
		if action == 0:
			statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)			
			os.system (statement)
		else:
			statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
	except Exception as e:
		if debug ==1:
			print(e)
  

counter = 0

while True:	
	try:	
		time.sleep(1)		
		currentday = datetime.datetime.today().weekday()
		ignore=''
		if debug==1:
			print ("\ntodays number is:",currentday)
		if debug == 1:
			counter = counter + 1
			# print(chr(27) + "[2J")	
		try:
			f = open("mynode.txt", "r")
			nodeselected = f.read()
			nodeselected = nodeselected.replace("\n","")
			f.close()
			if debug==1:		
				print (nodeselected)
			# print ("Node: "),(nodeselected),("Attempt:"),counter,"\n\n"


		# except Exception as e: print(e)	
		except:
			if debug == 1:
				print ("Failed to read mynode.txt")

		curs = db.cursor()
		curs.execute ('SELECT * FROM octavia.%s' % (nodeselected))
		# curs.execute ('SELECT * FROM node_Workshop')
		results = curs.fetchall()
		for row in results:
			idd = row[0]
			description = row[1]
			begin = row[2]
			end = row[3]
			objectname = row[4]
			csv=objectname.split(",")
			gpiocategory= csv[0]
			intime = row[5]
			outtime = row[6]		
			hold = row[7]
			polarity = row[8]
			print (polarity)
			actiondb = row[9]			
			day = row[10]
			now = datetime.datetime.now()
			now = (now.strftime("%H:%M:%S"))
			begin = (begin.strftime("%H:%M:%S"))
			end = (end.strftime("%H:%M:%S"))
			
			if gpiocategory == "esp":				
				gpioid = csv[1]
				# gpioid = row[4]				
				curs.execute ('SELECT * FROM octavia.esp WHERE id=%s' % (gpioid))
				resultsB = curs.fetchall()			
				for rowB in resultsB:
					gpio=rowB[2]
					polarity=rowB[3]
					esp=rowB[4]
				
			
		
				
				# if debug == 1:
				# 	print "The GPIO is = %s" % (gpio)
			
			
				force=0
				if hold == 0:
					hold="OFF"				
					relayoff(gpio,polarity,"NO")
					force=1
					# ignore=True
			
				if hold == 1:				
					hold="ON"
					relayon(gpio,polarity,"NO")
					force=1
					# ignore=True
			
				testday = any(i in str(currentday) for i in str(day))
			
			
				# if debug==1:
				# 	print testday,description,"day to be on is: ",day #Indicates relay and "day" it's meant to fire.
				if day=="8":
					testday=True
				if day=="":
					testday=False
					ignore=True			
				# breakpoint()
				if hold == 2 and testday==True:
					hold="AUTO"				
					if now > begin and now < end:
						if intime == 1:
							relayon(gpio,polarity,"NO")
						else:
							relayoff(gpio,polarity,"NO")				
					else:
						if outtime == 1:
							relayon(gpio,polarity,"NO")
						else:
							relayoff(gpio,polarity,"NO")
			
				if force==1:
					donothing=1
				else:
					if str(day)=="8":
						donothing=1 #Becuase 8 means daily and it should listen to the AUTO(hold=2) rule.
					else:
						if testday!=True:					
							relayoff(gpio,polarity,bcolors.WARNING + "YES" + bcolors.ENDC)
						donothing=1
	
	
				db.commit()	
		if debug==1:
			print (table)
			table.clear_rows()
	except Exception as e:
		if debug ==1:
			print(e)	
	# except:
		# Fail=1