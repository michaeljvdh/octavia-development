debug=0

import MySQLdb
import datetime
import RPi.GPIO as GPIO
import subprocess
import dbconnection
import time
from prettytable import PrettyTable
import os



dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)


f = open("mynode.txt", "r")
nodeselected = f.read()
nodeselected = nodeselected.replace("\n","")
nodeselected = nodeselected.replace("`","")
f.close()

# if debug==1:		
	# print (nodeselected)

now = datetime.datetime.now()
now = (now.strftime("%H:%M:%S"))





def relayon(x,y,gpiotype):
	# print ("--------------------------- %s,%s,%s" %(x,y,gpiotype))		
	if actiondb==0:		
		curs = db.cursor()
		curs.execute ('UPDATE octavia.%s SET action="1" WHERE objectname="%s"' % (nodeselected,objectname))
		if alert==1:
			curs.execute('INSERT INTO messages SET message="<strong>In Scheduled GPIO</strong>\n<strong>%s</strong> using GPIO %s known as <strong>%s</strong> has been powered <strong>ON</strong>.", node="%s";' %(description,gpio,gpiodescription,nodeselected))
		else:
			donothing=1		
		db.commit()
	action = 0
	if y == 1:
		action = 1		
	# table.add_row ([description,bcolors.OKGREEN +"ON "+ bcolors.ENDC,gpio,action,y,hold,begin,now,end,skip])
	# print (gpiotype)
	if gpiotype=="gpio":
		try:
			GPIO.setup(int(gpio), GPIO.OUT)		
			GPIO.output(int(gpio), int(action))
		except Exception as e:
			if debug==1:
				print(e)
	if gpiotype=="esp":
		try:
			if action==1:
				statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
				os.system (statement)
			else:
				statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)
				os.system (statement)
		except Exception as e:
			if debug ==1:
				print(e)

def relayoff(x,y,gpiotype):
	# print (".............................%s,%s,%s" %(x,y,gpiotype))	
	if actiondb==1:
		curs = db.cursor()
		curs.execute ('UPDATE octavia.%s SET action="0" WHERE objectname="%s"' % (nodeselected,objectname))
		if alert==1:
			curs.execute('INSERT INTO messages SET message="<strong>In Scheduled GPIO</strong>\n<strong>%s</strong> using GPIO %s known as <strong>%s</strong> has been powered <strong>OFF</strong>.", node="%s";' %(description,gpio,gpiodescription,nodeselected))
		else:
			donothing=1
		db.commit()
	action = 1
	if y == 1:
		action = 0
	# table.add_row ([description,bcolors.FAIL + "OFF" + bcolors.ENDC,gpio,action,y,hold,begin,now,end,skip])
	# print (gpiotype)
	if gpiotype=="gpio":
		try:
			GPIO.setup(int(gpio), GPIO.OUT)
			GPIO.output(int(gpio), int(action))		
		except Exception as e:
			if debug==1:
				print(e)
	
	if gpiotype=="esp":
		try:
			if action == 0:
				statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)			
				os.system (statement)
			else:
				statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
				os.system (statement)
		except Exception as e:
			if debug ==1:
				print(e)


while True:
	time.sleep(1)
	now = datetime.datetime.now()
	now = (now.strftime("%H:%M:%S"))
	# print (now)
	if now > "00:00:01" and now < "00:00:06":		
		curs.execute ("UPDATE %s SET once='0'"% (nodeselected))
		db.commit()	
	try:
		curs = db.cursor()
		curs.execute ('SELECT * FROM %s GROUP BY objectname ' % (nodeselected))
		db.commit()
		# curs.execute ('SELECT * FROM %s GROUP BY objectname HAVING COUNT(objectname)>1' % (nodeselected))
		results = curs.fetchall()		
		for row in results:
			idd = row[0]	
			objectname = row[4]
			objectname_csv=objectname.split(",")
			objecttype=objectname_csv[0]
			objecttype_number=objectname_csv[1]
			# hold = row[7]
			actiondb = row[8]
			once=row[13]
			onceoption=row[14]
			# day = row[9]
			
			currentday = datetime.datetime.today().weekday()

		
			curs.execute ('SELECT * FROM %s WHERE objectname="%s"' % (nodeselected,objectname))
			db.commit()
			results_b = curs.fetchall()		
			resultingaction = 0
			for row_b in results_b:
				alert = row[10]						
				description = row_b[1]
				begin = row_b[2]
				end = row_b[3]
				day = row_b[9]
				auto = row_b[7]
				cycleduration=row[11]				
				begin = (begin.strftime("%H:%M:%S"))
				end = (end.strftime("%H:%M:%S"))
				# print (description,begin,end)
				# print ('---%s--%s---%s-' %(day,currentday,description))				
				
				testday = any(i in str(currentday) for i in str(day))
				if testday==True or day == 8:
					day_continue="yes"
				else:
					day_continue="no"

				# print ("aasdfasdfas",day_continue,description)

				if now>begin and now <end and day_continue=="yes" or day=="8":
					resultingaction = 1		
				# if debug==1:
					# print (resultingaction)
		
				if objectname_csv[0] == "gpio":			
					curs.execute ('SELECT * FROM octavia.gpio WHERE id=%s' % (objectname_csv[1]))
					db.commit()				
					resultsB = curs.fetchall()				
					for rowB in resultsB:					
						gpio=rowB[2]
						polarity=rowB[3]
						isthis_gpio=1
						isthis_esp=0
		
				if objectname_csv[0] == "esp":			
					curs.execute ('SELECT * FROM octavia.esp WHERE id=%s' % (objectname_csv[1]))
					db.commit()				
					resultsB = curs.fetchall()				
					for rowB in resultsB:					
						gpio=rowB[2]
						polarity=rowB[3]
						isthis_gpio=0
						isthis_esp=1
						esp=rowB[4]
				
			
			if cycleduration==0: #Means that no cycleduration has been set and is 0 in SQL
				# print ('-----------------------------------------',resultingaction)
				if resultingaction == 1 and auto==2:		
						if objectname_csv[0] == "gpio":
							GPIO.setup(int(gpio), GPIO.OUT)					
							# print ("POWER ON GPIO %s" % (gpio))
							relayon(gpio,polarity,"gpio")
						if objectname_csv[0] == "esp":
							# print ("POWER ON ESP %s" % (gpio))
							relayon(gpio,polarity,"esp")
				if auto==1:
					if objectname_csv[0] == "gpio":
						GPIO.setup(int(gpio), GPIO.OUT)					
						# print ("POWER ON GPIO %s" % (gpio))
						relayon(gpio,polarity,"gpio")
					if objectname_csv[0] == "esp":
						# print ("POWER ON ESP %s" % (gpio))
						relayon(gpio,polarity,"esp")
				
				if resultingaction == 0 and auto ==2:	
						if objectname_csv[0] == "gpio":
							GPIO.setup(int(gpio), GPIO.OUT)
							# print ("POWER OFF GPIO %s" %(gpio))
							relayoff(gpio,polarity,"gpio")
						if objectname_csv[0] == "esp":
							# print ("POWER OFF ESP %s" % (gpio))
							relayoff(gpio,polarity,"esp")
				if auto==0:
					if objectname_csv[0] == "gpio":
						GPIO.setup(int(gpio), GPIO.OUT)
						# print ("POWER OFF GPIO %s" %(gpio))
						relayoff(gpio,polarity,"gpio")
					if objectname_csv[0] == "esp":
						# print ("POWER OFF ESP %s" % (gpio))
						relayoff(gpio,polarity,"esp")
		
			
			else:
				curs = db.cursor()
				curs.execute ('SELECT * FROM %s WHERE cycleduration > "0"' % (nodeselected))
				db.commit()
				# curs.execute ('SELECT * FROM %s GROUP BY objectname HAVING COUNT(objectname)>1' % (nodeselected))
				resultsC = curs.fetchall()		
				for rowC in resultsC:
					idd = rowC[0]
					auto = rowC[7]
					day = rowC[9]
					testday = any(i in str(currentday) for i in str(day))
					if testday==True or str(day) == "8":
						day_continue="yes"
					else:
						day_continue="no"					

					setup_variableA = "duration_%s" % (idd)
					globals()[setup_variableA] = rowC[11]
					setup_variableB = "rest_%s" % (idd)
					globals()[setup_variableB] = rowC[12]
					setup_variableC = "total_%s" % (idd)
					globals()[setup_variableC] = rowC[11]
					setup_variableCount = "count_%s" % (idd)
					globals()[setup_variableC] = rowC[11]					
					objectname = rowC[4]
					objectname_csv=objectname.split(",")
					objecttype=objectname_csv[0]
					objecttype_number=objectname_csv[1]					
					curs.execute ('SELECT * FROM %s WHERE id=%s' % (objecttype,int(objecttype_number)))
					db.commit()
					resultsD = curs.fetchall()					
					for rowD in resultsD:												
						gpio = rowD[2]
						# print ('GPIO TEST',gpio)
						# time.sleep(10)					
						polarity = rowD[3]
						# print ("xxx",gpio,polarity)
						(globals()["total_%s"%(idd)]) = (globals()["duration_%s"%(idd)]) + (globals()["rest_%s"%(idd)])	
						nowcycle = time.time()
						# if debug==1:
							# print ("-------------------------------------------------------")
						path = "/mnt/octavia/%scycle.start"%(idd)							
						pathdynamic = "/mnt/octavia/%sdynamic.start"%(idd)	
						count = "/mnt/octavia/%scount.start"%(idd)

						# print ("=======================================")
						# print (path,gpio)
						# print ("=======================================")
				
						test_start = os.path.exists(path)
						test_dynamic = os.path.exists(pathdynamic)	
				
						if test_start==False:
							f = open(pathdynamic, "w")
							dynamicanswer = f.write('1')
							f.close()
							os.chmod(pathdynamic, 0o777)
						
						if test_dynamic==False:
							f = open(path, "w")
							timestamp = f.write('%s'%(nowcycle))
							f.close()
							os.chmod(path, 0o777)
						
						f = open(pathdynamic, "r")
						dynamicanswer = f.read()
						f.close()		
				
						if int(dynamicanswer) == 0:
							f = open(path, "w")
							timestamp = f.write('%s'%(nowcycle))
							f.close()								
						
						f = open(pathdynamic, "w")
						dynamicanswer = f.write('1')
						f.close()
						
						f = open(path, "r")
						started = f.read()	
						f.close()
													
						# if debug==1:
						# 	print ("started: ",started, gpio)
						# 	print ("now: ",nowcycle)	
				
						elapsed = nowcycle - float(started) +1	
				
						# if debug==1:
						# 	print ("elapsed: ",int(elapsed))		
						
						if once==0:
							if auto ==2 and day_continue == "yes":
								# print ('=======================================================================================')					
								if int(elapsed) <= (globals()["duration_%s"%(idd)]):
									# if debug==1:
										# print ("POWER ON ----- CYCLE")									
									relayon(gpio,polarity,"%s"%(objecttype))											
					
								if int(elapsed) > (globals()["duration_%s"%(idd)]) and int(elapsed) <= (globals()["total_%s"%(idd)]):
									# if debug==1:									
										# print ("POWER OFF <<<<< CYCLE")									
									relayoff(gpio,polarity,"%s"%(objecttype))													
									if onceoption==1:
										curs.execute ("UPDATE %s SET once=1 WHERE id='%s'"%(nodeselected,idd))
										db.commit()
					
								# if debug==1:
									# print ((globals()["total_%s"%(idd)]),int(elapsed))
								if int(elapsed) > (globals()["total_%s"%(idd)]):
									if debug==11:										
										print ("YES")
									
					
								if int(elapsed) >= (globals()["total_%s"%(idd)]):
									# if debug==1:
										# print ("TRUE HERE")								
									(globals()["dynamic_count_%s"%(idd)])=0
									f = open(pathdynamic, "w")
									data = f.write('0')
									f.close()
							else:
								if auto==1:
									relayon(gpio,polarity,"%s"%(objecttype))
								if auto==0:
									relayoff(gpio,polarity,"%s"%(objecttype))
						else:
							skipped="yes"
	
	except Exception as e:
		if debug==1:
			print(e)





