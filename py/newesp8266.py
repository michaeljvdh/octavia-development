debug=0
import json
import MySQLdb
import dbconnection
import time
from prettytable import PrettyTable # sudo pip3 install PTable
# import numpy
nodeselected = "masterrelay"
dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()



# print (innerresults)

import sqlite3
sqliteConnection = sqlite3.connect("/mnt/octavia/new1.db")
sqliteCursor = sqliteConnection.cursor()
sqliteCursor.execute("CREATE TABLE IF NOT EXISTS 'sensors' (	'id'	INTEGER,	'fromtable'	TEXT,'sensortype'	TEXT,'table_id'	INTEGER,	'description'	TEXT,	'uid'	TEXT UNIQUE,	'temperature'	REAL DEFAULT nan,	'humidity'	REAL DEFAULT nan,	'pressure'	REAL DEFAULT nan,	'altitude'	REAL DEFAULT nan,	PRIMARY KEY('id' AUTOINCREMENT));")
sqliteConnection.commit()

checkpoint = time.time()
while True:
	try:
		curs = db.cursor()
		curs.execute ('SELECT * FROM config where description="log" and set1="global";')
		db.commit()
		results = curs.fetchall()
		for row in results:
			loginterval=row[3]
			loginterval = int(loginterval)
		
		curs.execute ('SELECT * FROM bme where node="%s"'%(nodeselected))
		innerresults = curs.fetchall()
		db.commit()
		for innerrow in innerresults:
			fromtable = "bme"
			# print ("Indo: ",innerrow)
			id_id= innerrow[0]
			uid = innerrow[2] #serial number
			# sensorvalidate.append(uid)
			# print (uid,".json")
			
			table_id = innerrow[0]
			description = innerrow[1]	
			prep = ('/mnt/octavia/%s.json' %(uid))
			with open(prep, 'r') as fj:
				data = json.load(fj)
			# print (data,"\n")
			# print (data[8])
			
			# print (description)
			sensor1 = data[8]
			sensortype = (sensor1['sensor'])			
			temperature = (sensor1['temperature'])
			try:
				humidity = (sensor1['humidity'])
			except:
				humidity = 'nan'
			try:
				pressure = (sensor1['pressure'])
			except:
				pressure = 'nan'
			try:
				altitude = (sensor1['altitude'])
			except:
				altitude = 'nan'

			if debug==1:
				print (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude)
			# exit()

			sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype, table_id,description,uid,temperature,humidity,pressure,altitude) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude))
			sqliteConnection.commit()
			sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s',pressure='%s',altitude='%s' WHERE uid='%s' AND sensortype='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude,uid,sensortype))
			sqliteConnection.commit()

			if loginterval!=0 and time.time() - checkpoint > loginterval:

				if sensortype == "ds18b20":
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','ds18b20');" % (id_id,nodeselected,float(temperature)))
					db.commit()						

				if sensortype == "dht":
					
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','dht');" % (id_id,nodeselected,float(temperature)))
					db.commit()
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','humidity','%s','%s','dht');" % (id_id,nodeselected,float(humidity)))
					db.commit()
				if sensortype == "bme":					
					# if debug==1:
					# print ("Processing BME")
					# print ("catch2")
					# print(temperature)					
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','bme');" % (id_id,nodeselected,float(temperature)))
					db.commit()
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','pressure','%s','%s','bme');" % (id_id,nodeselected,float(pressure)))
					db.commit()
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','humidity','%s','%s','bme');" % (id_id,nodeselected,float(humidity)))
					db.commit()
					curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','altitude','%s','%s','bme');" % (id_id,nodeselected,float(altitude)))
					db.commit()


			# print (sensor1)
			try:
				sensor2 = data[9]		
				sensortype = (sensor2['sensor'])
				uid = uid+"(b)"
				temperature = (sensor2['temperature'])
				try:
					humidity = (sensor2['humidity'])
				except:
					humidity = 'nan'
				try:
					pressure = (sensor2['pressure'])
				except:
					pressure = 'nan'
				try:
					altitude = (sensor2['altitude'])
				except:
					altitude = 'nan'
				if debug==1:
					print (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude)
				sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype, table_id,description,uid,temperature,humidity,pressure,altitude) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude))
				sqliteConnection.commit()
				sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s',pressure='%s',altitude='%s' WHERE uid='%s' AND sensortype='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude,uid,sensortype))
				sqliteConnection.commit()
				if loginterval!=0 and time.time() - checkpoint > loginterval:
					try:
						if debug==1:
							print ("trying",sensortype)
						if sensortype == "ds18b20":
							curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','ds18b20');" % (id_id,nodeselected,float(temperature)))
							db.commit()
					except:
						if debug==1:
							print ("Pass")
			except:			
				pass
			sensortype=''
			

	except Exception as e:
				if debug==1:				
					print(e)		
	if time.time() - checkpoint > loginterval :
				checkpoint = time.time()
	if debug==1:
		try:
			table = PrettyTable()
			table.field_names = ["1","2","3","4","5","6","7","8","9","10"]
			rows = sqliteCursor.execute("SELECT * FROM sensors").fetchall()
			sqliteConnection.commit()
			for x in rows:
				table.add_row([x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9]])			
			print (table)
			
			table=''
			rows=''
		except:
			pass
	time.sleep(2)