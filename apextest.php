<style type="text/css">
#chart {
  /*max-width: 650px;*/
  margin: 35px auto;
}
</style>
<!-- <link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/uk-style-light.css" /> -->

<script src="js/apexcharts.js"></script>
<?php
include "connection.php";
// include "header.php";
// include "nav.php";
$unique=mt_rand();

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
$thisnode = fgets($myfile);
$thisnode = str_replace('`', '', $thisnode);
fclose($myfile);
$thisnode = trim($thisnode);
$thislocalnode = $thisnode;
$thislocalnode = str_replace('masterrelay', 'Master Pi', $thislocalnode);

$stmt = $db->query("SELECT * FROM config WHERE description='log' AND node='$thisnode';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  if ($row['set1']=='ds18b20'){$ds18b20 = $row['set4'];};
  if ($row['set1']=='dht1122'){$dht1122 = $row['set4'];};
  if ($row['set1']=='bme'){$bme = $row['set4'];};

  };

$value=187;
print '<div class="apexsize"><div id="chart'.$unique.'"></div></div>';

$attr = 'temperature';
$table_id = 341;
$temperature_array = array();
$dht1122_array = array();
$pressure_array = array();
$altitude_array = array();
$time_array = array();

$db = new SQLite3('/mnt/octavia/test.db');
$db->busyTimeout(2000);
$stmt = $db->query("SELECT * FROM sensors  ORDER BY fromtable ASC, description DESC;");
while ($row = $stmt->fetchArray()) {
    $id=$row['id'];
    $description=$row['description'];
    $temperature=$row['temperature'];
    $humidity=$row['humidity'];
    $pressure=$row['pressure'];
    $altitude=$row['altitude'];    
	    if ($humidity=="nan") {$humidity=0;};
    	if ($pressure=="nan") {$pressure=0;};
    	if ($altitude=="nan") {$altitude=0;};    
    $time=date("h:i:sa");
        $time = "'".$description." @ ".$time."'";
    $description_format = '"'.$description.'"';
    	if ($value=="nan") {;}else {
    array_push($temperature_array, $temperature);    
    array_push($dht1122_array, $humidity);
    array_push($pressure_array, $pressure);
    array_push($altitude_array, $altitude);
    array_push($time_array, $description_format);    
	};
};

$temperature_array = array_reverse($temperature_array);
$dht1122_array = array_reverse($dht1122_array);
$time_array = array_reverse($time_array);
$data = implode($temperature_array,",");
$data2 = implode($dht1122_array,",");
$data3 = implode($pressure_array,",");
$labels = implode($time_array,",");
print '
<script type="text/javascript">
	var options'.$unique.' = {
grid: {
	xaxis: {
		lines: {
			show: true,
			}	
		},
      },

  chart: {
  	animations: {
        enabled: false,
        easing: \'easeinout\',
        speed: 800,
        animateGradually: {
            enabled: true,
            delay: 150
        },
        dynamicAnimation: {
            enabled: true,
            speed: 350
        }
    	},
    type: \'bar\',
    stacked: true,
    plotOptions: {
          bar: {
            horizontal: true,
          },
        },
    toolbar: {
    	show: false,
    	 tools: {
          download: true,
          selection: true,
          zoom: true,
          zoomin: true,
          zoomout: true,
          pan: true,
          },
    },
  },

  series: [
  {
    name: \'Temperature:\',
    data: ['.$data.'],
  },
  {
    name: \'Humidity:\',
    data: ['.$data2.'],
  },
  ],

yaxis: {
  opposite: true,
},
  xaxis: {    
    categories: ['.$labels.']
  },
	stroke: {
  		curve: \'smooth\',
  		width:2,
  	},
	colors: [\'#77B6EA\', \'#545454\'],
	dataLabels: {
	          enabled: true,
    	    },          
	}
var chart'.$unique.' = new ApexCharts(document.querySelector("#chart'.$unique.'"), options'.$unique.');

chart'.$unique.'.render();
</script>
';
?>
