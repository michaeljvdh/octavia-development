<style type="text/css">


#chart {
  /*max-width: 650px;*/
  /*width: 1024px;*/
  margin: 35px auto;
  /*background-color: red;*/
}


.apexcharts-tooltip {
    
    color: black;
  }

</style>

<script src="js/apexcharts.js"></script>
<br>
<div align="center">
<div class="uk-card-body" style="max-width: 80%;">
<?php
include "connection.php";
include "header.php";

if ($theme=="uk-style-dark"){
  $labelcolor = "white";
  $apexline = "red";
} else {$labelcolor="black";
$apexline = "black";};

$unique=mt_rand();

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
$thisnode = fgets($myfile);
$thisnode = str_replace('`', '', $thisnode);
fclose($myfile);
$thisnode = trim($thisnode);
$thislocalnode = $thisnode;
$thislocalnode = str_replace('masterrelay', 'Master Pi', $thislocalnode);


$stmt = $db->query("SELECT * FROM config WHERE description='log' AND set1='global';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  // if ($row['set1']=='ds18b20'){$ds18b20 = $row['set4'];};
  // if ($row['set1']=='dht1122'){$dht1122 = $row['set4'];};
  // if ($row['set1']=='bme'){$bme = $row['set4'];};
    $display = $row['set4'];

  };


// $record_collection = array();


// $stmt = $db->query("SELECT * FROM ds18b20;");
//   while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
//     array_push($record_collection, $row['id']);    
//   };
$value=187;


$table = $_GET['table'];
$sensortype=$_GET['sensortype'];
$attr = $_GET['attr'];
$type = $attr;
$label = ucfirst($attr);
$table_id = $_GET['table_id'];
$displaytype = $_GET['displaytype'];
$description = $_GET['description'];
print "<div align='center'><strong style='font-size:1.5em;'>".$description."</strong>-<font style='font-size:1.5em;'>".$label."</font></div>";
print '<div align="center"><div class="apexsize"><div class="" id="chart'.$unique.'"></div></div>';

// if ($displaytype=='ds18b20') {$display=$ds18b20;};
// if ($displaytype=='dht1122') {$display=$dht1122;};
// if ($displaytype=='bme') {$display=$bme;};

// $table='ds18b20';
// $attr = 'temperature';
// $table_id = 187;


$ds18b20_array = array();
$time_array = array();
$stmt = $db->query("SELECT * FROM log WHERE `table`='$table' and attr='$attr' and table_id='$table_id' and sensor='$sensortype' ORDER BY id DESC LIMIT $display;");
  


  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    
    $valued = $row['value'];
    $time = $row['stamp'];
    $attrname = $row['attr'];

    $time = strtotime($time);
    $time = date("H:i:s",$time);
    $valued = $valued;
    $time = "'".$time."'";
    array_push($ds18b20_array, $valued);    
    array_push($time_array, $time);    
  };

$ds18b20_array = array_reverse($ds18b20_array);
$time_array = array_reverse($time_array);

$data = implode($ds18b20_array,",");
$labels = implode($time_array,",");

print '
<script type="text/javascript">
	var options'.$unique.' = {
grid: {
	xaxis: {
		lines: {
			show: true,
			}	
		},
      },

  chart: {
    foreColor: \''.$labelcolor.'\',
    height: \'300\',
     animations: {
        enabled: false,
        easing: \'easeinout\',
        speed: 500,
        animateGradually: {
            enabled: true,
            delay: 500
        },

             
        dynamicAnimation: {
            enabled: false,
            speed: 10
        }
    },
    type: \'area\',
    toolbar: {
    	show: true,
    	 tools: {
          download: true,
          selection: true,
          zoom: true,
          zoomin: true,
          zoomout: true,
          pan: true,
          },

    },
  },

  series: [
  {
    name: \''.$attr.':\',
    data: ['.$data.'],

  },

 

  ],

yaxis: {
  opposite: true, 
  labels: {
       show: true,
       

          },


},

  xaxis: {    
    categories: ['.$labels.'],
labels: {
       show: true,
       

          },

      },

	stroke: {
  		curve: \'smooth\',
  		width:2,
  	},

	colors: [\''.$apexline.'\', \''.$apexline.'\'],

	dataLabels: {
	          enabled: false,

    	       },          
	


  }

var chart'.$unique.' = new ApexCharts(document.querySelector("#chart'.$unique.'"), options'.$unique.');

chart'.$unique.'.render();
</script>

';
?>

<style>
   iframe {
      /*width: 90%;*/
      /*max-width: 800px;*/
      /*height: 100%;*/
       overflow:hidden;
    }
</style>
<!-- <div class="uk-card uk-card-default uk-card-body"> -->
<!-- <iframe src="live.php?description=<?php print $description;?>&type=<?php print $type;?>" name="iframeapex" style="overflow: hidden; height: 300px; width:100%; "></iframe> -->
<!-- </div> -->
</div>
</div>