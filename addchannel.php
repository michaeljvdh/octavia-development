<?php
$noderequest = $_GET['node'];
include "connection.php";
include "header.php";
include "nav.php";

$addpitables = array();
array_push($addpitables, 'masterrelay');
$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$tablename = $row['Tables_in_octavia'];
		array_push($addpitables, $tablename);
	;};

$onchange = "";
$noderequest_strip = str_replace("node_", "", $noderequest);
$noderequest_strip = str_replace("_", " ", $noderequest_strip);


$gpio_id_array = array();
$stmt = $db->query("SELECT id FROM gpio WHERE node='$thisnode';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $id = ("gpio,".$row['id']."");
    array_push($gpio_id_array, $id);
};


$esp_id_array = array();
$stmt = $db->query("SELECT id FROM esp WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("esp,".$row['id']."");
            array_push($esp_id_array, $id);
        };



?>
<style type="text/css">
	.stylemyinput_checkbox {
	min-height: 30px;
	min-width: 30px;		
	}
	th {
		text-align: center;
	}
 :focus {outline: none !important;}
</style>

<div class="container">
	<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Add a Scheduled Item</h3>
<!-- Accordian open -->

<!-- Data -->
<div class="<?php print $theme;?>"  >
	<form class="" name = "addcustomrelay" action="submit.php" method="POST" style="margin:10px;padding-bottom: 10px;">
		<table class="uk-table " id="tbl_posts">
				<input name="option" value = "addcustomrelay" hidden>
				<input name="fromnode" value="schedulegpio.php" hidden>
				<tr>
					<thead>
							<th>Description</th>
							
							<th>Gpio</th>							
							<th>Node</th>
							<th hidden>If <strong>in</strong>  zone</th>
							<th hidden>If <strong>out</strong> zone</th>
							<th>Auto</th>
							<th>Begin Zone</th>
							<th>End Zone</th>		
					</thead>
				</tr>
			<tbody id="tbl_posts_body">
			<tr id="rec-1">
<!-- REPLICATION PORTION -->
				<td><input required class="uk-input" name="description[]"></td>
				
				
				<?php
				print '<td><select class="uk-select" name="gpio_id[]" required>';

				foreach($gpio_id_array as $key => $value) {
					$x = explode(",",$value);                    
					if ($object == $value){$selected = "selected";}else{$selected="";};
						$stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
							while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
										$gpioNumber=$row2['number'];
										$gpioDescription=$row2['description'];                                    
								};               
					print '<option value="'.$value.'" '.$selected.'>GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
						};

				foreach($esp_id_array as $key => $value) {
    $x = explode(",",$value);                    
    if ($object == $value){$selected = "selected";}else{$selected="";};
        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    $espNumber=$row2['number'];
                    $espDescription=$row2['description'];                                    
                };               
         print '<option value="'.$value.'" '.$selected.'>ESP: '.$espNumber.' ('.$espDescription.')</option>';
            };
				
				print '</select></td>';
				
				?>
				<td><select name="node[]" class="uk-select">
				<?php
					foreach ($addpitables as $key => $value) {
						$correctedvalue = str_replace("node_", "", $value);
						$correctedvalue = str_replace("_", " ", $correctedvalue);
						if ($value=="masterrelay") {$correctedvalue="Master Node";};
						if ($value==$noderequest) {$selected="selected";} else {$selected="";};
						print '<option class="" value="'.$value.'" '.$selected.'>'.$correctedvalue.'</option>';
						};
				?>						
					</select>
				</td>
				<td hidden>
					<select name="ifinzone[]" class="uk-select">
						<option value="1">ON</option>
						<option value="0">OFF</option>
					</select>
				</td>
				<td hidden>
					<select name="ifoutzone[]" class="uk-select">
						<option value="0">OFF</option>
						<option value="1">ON</option>
						
					</select>
				</td>
				<td>
					<select name="pause[]" class="uk-select">
						<option value="2">AUTO</option>
						<option value="1">ON</option>
						<option value="0">OFF</option>
					</select>
				</td>
				<td><input required step="1" type="time" class="uk-input" name="begin[]" value="08:00"></td>
					<td><input required step="1" type="time" class="uk-input" name="end[]" value="17:00"></td>
					<td  style="text-align: center;"><a class="add-record" data-added="0"><div style="position:relative;top:-5px;padding-bottom:0px;font-size:2em;width:100%;color: lightgreen;">+</div></a></td>
<!-- REPLICATION PORTION -->
          		</tr>
			</tbody>
			
			</table>
			<input type="text" value="master" name="fromurl" hidden>
			<button class="<?php print $theme;?> uk-button uk-button-default save-button" type="submit">Save</button>
		</form>


</div>
       
       <br>

<div style="display:none;">
    <table id="sample_table">
      <tr id="">
<!-- REPLICATION PORTION -->
				<td><input required class="uk-input" name="description[]"></td>
				
					<?php
				print '<td><select class="uk-select" name="gpio_id[]">';

				foreach($gpio_id_array as $key => $value) {
					$x = explode(",",$value);                    
					if ($gpio_id == $x[1]){$selected = "selected";}else{$selected="";};
						$stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
							while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
										$gpioNumber=$row2['number'];
										$gpioDescription=$row2['description'];                                    
								};               
					print '<option value="'.$x[1].'" '.$selected.'>GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
						};

				foreach($esp_id_array as $key => $value) {
    $x = explode(",",$value);                    
    if ($object == $value){$selected = "selected";}else{$selected="";};
        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    $espNumber=$row2['number'];
                    $espDescription=$row2['description'];                                    
                };               
         print '<option value="'.$value.'" '.$selected.'>ESP: '.$espNumber.' ('.$espDescription.')</option>';
            };
				
				print '</select></td>';
				
				?>
				
				<td><select name="node[]" class="uk-select">
						<!-- <option value="masterrelay">masterrelay</option> -->
							<?php
							foreach ($addpitables as $key => $value) {
								$correctedvalue = str_replace("node_", "", $value);
								$correctedvalue = str_replace("_", " ", $correctedvalue);
								if ($value=="masterrelay") {$correctedvalue="Master Node";};
							if ($value==$noderequest) {$selected="selected";} else {$selected="";};
							print '<option class="" value="'.$value.'" '.$selected.'>'.$correctedvalue.'</option>';
							};


							?>						
					</select>
				</td>
				<td hidden>
					<select name="ifinzone[]" class="uk-select">
						<option value="1">ON</option>
						<option value="0">OFF</option>
					</select>
				</td>
				<td hidden>
					<select name="ifoutzone[]" class="uk-select">
						<option value="1">ON</option>
						<option value="0">OFF</option>
					</select>
				</td>
				<td>
					<select name="pause[]" class="uk-select">
						<option value="2">AUTO</option>
						<option value="1">ON</option>
						<option value="0">OFF</option>
					</select>
				</td>
				<td><input required type="time" class="uk-input" name="begin[]"></td>
				<td><input required type="time" class="uk-input" name="end[]"></td>
				<td  style="text-align: center;"><a class="btn btn-xs delete-record" data-id="0"><i class="uk-icon-link" uk-icon="trash"  style="color: red;"></a></td>
<!-- REPLICATION PORTION -->
     </tr>     
   </table>
 </div>

<div class="uk-animation" tabindex="0"><!-- Animation Start -->
    <div class="uk-animation-scale-up uk-transform-origin-top-center"><!-- Animation Start -->

<script type="text/javascript">
	jQuery(document).delegate('a.add-record', 'click', function(e) {
		e.preventDefault();    
		var content = jQuery('#sample_table tr'),
		size = jQuery('#tbl_posts >tbody >tr').length + 1,
		element = null,    
		element = content.clone();
		element.attr('id', 'rec-'+size);
		element.find('.delete-record').attr('data-id', size);
		element.appendTo('#tbl_posts_body');
		element.find('.sn').html(size);
	});

	jQuery(document).delegate('a.delete-record', 'click', function(e) {
		e.preventDefault();    
		// var didConfirm = confirm("Are you sure You want to delete");
		// if (didConfirm == true) {
			if (1 == 1) {
		var id = jQuery(this).attr('data-id');
		var targetDiv = jQuery(this).attr('targetDiv');
		jQuery('#rec-' + id).remove();
		
		//regnerate index number on table
		$('#tbl_posts_body tr').each(function(index) {
		//alert(index);
		$(this).find('span.sn').html(index+1);
		});
		return true;
	} else {
		return false;
	}
	});
</script>
</div>