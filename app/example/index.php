<!doctype html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>jQuery Weely Scheduler Examples</title>
  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="css/doc.css" rel="stylesheet" />
  <link href="../src/css/scheduler.css" rel="stylesheet" />
  <style>
    body { background: #fafafa; font-family: 'Open Sans'; }
    .main {
      /*max-width: 800px;*/
      /*margin: 150px auto;*/
    }
  </style>
</head>



  <div class="main">    
    <table id="test1"></table>
    
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="../src/js/scheduler.js"></script>


<?php
include "../../connection.php";

$addpitables = array();
array_push($addpitables, 'masterrelay');
$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    $tablename = $row['Tables_in_octavia'];
    array_push($addpitables, $tablename);
  ;};

// print '<hr>';
// print_r($addpitables);
// print '<hr>';


foreach ($addpitables as $key => $nodes) {
  // print $nodes;
$starttimearray = array();
$starttimeminutesarray = array();
$endtimearray = array();
$endtimeminutesarray = array();
$descriptionarray = array();
$esparray = array();
$dayarray = array();
 
$stmt = $db->query('SELECT * FROM '.$nodes.' ORDER BY time(beginning),day;');
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                                      $beginining=$row['beginning'];
                                      $ending=$row['ending'];
    
    $id = $row['id'];
    $description = $row['description'];
    
    $begin = strtotime($beginining);
    $end = strtotime($ending);
    $aaa = date("Y-m-d",$begin); 
    $bbb = date("H:i:s",$begin); 
    $beginmod = $aaa." ".$bbb;
    $begintime = $bbb;
    $ccc = date("Y-m-d",$end);
    $ddd = date("H:i:s",$end);
    $endmod = $ccc." ".$ddd;
    $endtime = $ddd;
    $day = $row['day'];
    $dayarray = str_split($day);   

    $starthour = $begintime[0].$begintime[1];
    $startmin = $begintime[3].$begintime[4];
    $endhour = $endtime[0].$endtime[1];
    $endmin = $endtime[3].$endtime[4];

print '<Br>';

if( $endmin >=1  and $endmin <=15) {$addend=1;};
if( $endmin >=16 and $endmin <=30) {$addend=2;};
if( $endmin >=31 and $endmin <=45) {$addend=3;};
if( $endmin >=46 and $endmin <=59) {$addend=4;};
if( $endmin ==00) {$addend=0;};


print '
    <section>      
      <table id="test'.$id.'"></table>
      <script>
        $(\'#test'.$id.'\').scheduler({
          // multiple: false,          
          footer: false,
          disabled: true,
          accuracy: 4,
          data: {';
            ?>
            <?php 
            foreach ($dayarray as $key => $value) {
              
              $value = $value+1;
             print ''.$value.':[';
                foreach (range($starthour*4,$endhour*4-1+$addend) as $number){
                    print $number.',';
                     };# code...
                       print '],';

            };
            ?>
            <?php
            print '
          }
        });
      </script>
      <code>
      </code>
    </section>
    ';

  };        
};

?>

