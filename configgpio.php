<?php

include "connection.php";
include "header.php";
include "nav.php";

// $myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
// 		$thisnode = fgets($myfile);
// 		$thisnode = str_replace('`', '', $thisnode);
// 		fclose($myfile);
// 		$thisnode = trim($thisnode);
//         $value=$thisnode;

?>
<div class="uk-container">

<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">GPIO Management (Raspberry Pi)</h3>

<form action="submit.php" method="POST">
    <input name="option" value="gpioupdate" hidden>
<input id="" name="frompage" value="configgpio.php" hidden >
<?php
// ADD SENSORS TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
print '<div class="ukTableCard">';

print '<div class="">
<div class="container">
<div class="uk-button uk-button-default save-button" onclick="window.location.href =\'addgpio.php\';">Add GPIO</div><br>
<table class="uk-table">
<thead>
<th>GPIO Number</th>
<th>Description</th>
<th>Polarity</th>
<th style="text-align:center;color: red;max-width:1px;">DEL</th>
</thead>';
$stmt3 = $db->query("SELECT * from gpio WHERE node='$thisnode';");
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
        $id = $row3['id'];
        $gpioNumber = $row3['number'];
        $gpioDescription = $row3['description'];
        $gpioPolarity = $row3['polarity'];
        

print '
<input name="id[]" value="'.$id.'" hidden>
<tr>
<td><input class="uk-input" name="gpioNumber[]" value="'.$gpioNumber.'" type="number"></td>
<td><input class="uk-input" name="gpioDescription[]" value="'.$gpioDescription.'"></td>
<td><input class="uk-input" name="gpioPolarity[]" value="'.$gpioPolarity.'" min="0" max ="1" type="number"></td>
<td style="width:20px !important;"><input  class="uk-checkbox delete-checkbox-color" type="checkbox" name="gpioRemove[]" value="'.$id.','.$thisnode.'"</td>
</tr>
';

};
print '
</table>

<button class= "uk-button uk-button-default save-button" type="submit">UPDATE</button>
</div>
</div>
</div>';
// ADD SENSORS TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>
</form>
</div></div>
