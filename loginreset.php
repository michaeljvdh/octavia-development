<?php

include "connection.php";
include "header.php";
include "nav.php";

?>

<div class="uk-container">
	<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Login Code Reset</h3>

<script type="text/javascript">

  function checkForm(form)
  {
   

    if(form.pwd1.value != "" && form.pwd1.value == form.pwd2.value) {
      if(form.pwd1.value.length < 6) {
        alert("Error: Password must contain at least six characters!");
        form.pwd1.focus();
        return false;
      }
      if(form.pwd1.value == form.username.value) {
        alert("Error: Password must be different from Username!");
        form.pwd1.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(form.pwd1.value)) {
        alert("Error: password must contain at least one number (0-9)!");
        form.pwd1.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.pwd1.value)) {
        alert("Error: password must contain at least one lowercase letter (a-z)!");
        form.pwd1.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.pwd1.value)) {
        alert("Error: password must contain at least one uppercase letter (A-Z)!");
        form.pwd1.focus();
        return false;
      }
    } else {
      alert("Error: Please check that you've entered and confirmed your password!");
      form.pwd1.focus();
      return false;
    }

    alert("You entered a valid password: " + form.pwd1.value);
    return true;
  }

</script>







<form method="POST" action="submit.php" onsubmit="return checkForm(this);">
	<input name="option" value="loginreset" hidden>
		<table>
			<tr><td>New Code:</td><td><input class="uk-input uk-form-width-medium" type="password" name="pwd1"></td></tr>
			<tr><td>Confirm:</td><td><input class="uk-input uk-form-width-medium"  type="password" name="pwd2"></td></tr>
		</table>
	<p><button type="submit" class="uk-button uk-button-default save-button">SAVE</button></p>
</form>
</div></div>
