<?php 
include "connection.php";
include "header.php";
include "nav.php";
?>

<style>
	
</style>

<div class="uk-container">

<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Messages Sent or Pending Condition Change</h3>
    <p>Backlog 48hrs max (Records older than 48hrs are automatically dropped at 11PM daily)</p>
    
    <div align="right">
	    <form action="submit.php" method="POST" style="display: inline-table;">
	    	<input name="node" value="<?php print $thisnode; ?>" hidden>
	   	<input name="option" value="telegramdelete24" hidden>
	   	<button class="uk-button uk-button-danger" style="">DELETE OLDER THAN 24hrs</button>
	    </form>
	    <form action="submit.php" method="POST" style="display: inline-table;">
	    	<input name="node" value="<?php print $thisnode; ?>" hidden>
	   	<input name="option" value="telegramdelete48" hidden>
	      	<button class="uk-button uk-button-danger" style="">DELETE OLDER THAN 48hrs</button>
	    </form>
	    <form action="submit.php" method="POST" style="display: inline-table;">
	    	<input name="node" value="<?php print $thisnode; ?>" hidden>
	    	<input name="option" value="telegramdeleteall" hidden>    	
	    	<button class="uk-button uk-button-danger" style="">DELETE ALL</button>
	    </form>
	</div>

<table class="uk-table uk-table-striped">
<tr>
	<th>Id</th>
	<th>Message</th>
	<th>Status</th>
	<th>Sent</th>
</tr>
<?php
$stmt = $db->query("DELETE FROM messages WHERE DATE(timesent) NOT BETWEEN CURDATE()-1 AND CURDATE();");


$stmt = $db->query("SELECT * FROM messages WHERE node='$thisnode' ORDER BY ID DESC;");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$id = $row['id'];
		$message = $row['message'];
		$archive = $row['archive'];
		$timesent = $row['timesent'];
		$time = date("F j, Y, g:i a",strtotime($timesent));
		if ($archive==1) {$archive="Sent";} else {$archive="Pending";};
		print'
		
		<tr><td>'.$id.'</td><td>'.$message.'</td><td>'.$archive.'</td><td>'.$time.'</td></tr>
		

		';
	};
?>
</table>
</div>
</div>
