import MySQLdb
import dbconnection
import time
dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()
import Adafruit_DHT
sensor = Adafruit_DHT.DHT22
pin = 14
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
#curs.execute ('UPDATE octavia.%s SET action="1" WHERE id=%s' % (nodeselected,idd))
#db.commit()


import Adafruit_BMP.BMP085 as BMP085
sensor = BMP085.BMP085()

store = ('{0:0.2f}'.format(sensor.read_pressure()))
store = float(store) / 100

count = 0

while True:
	# print('Temp = {0:0.2f} *C'.format(sensor.read_temperature()))
	# print('Pressure = {0:0.2f} Pa'.format(sensor.read_pressure()))
	# print('Altitude = {0:0.2f} m'.format(sensor.read_altitude()))
	# print('Sealevel Pressure = {0:0.2f} Pa'.format(sensor.read_sealevel_pressure()))
	pressure = ('{0:0.2f}'.format(sensor.read_pressure()))
	seapressure = ('{0:0.2f}'.format(sensor.read_sealevel_pressure()))
	temp = ('{0:0.2f}'.format(sensor.read_temperature()))
	alt = ('{0:0.2f}'.format(sensor.read_altitude()))

	time.sleep(1)
	count = count + 1

	kpa = float(pressure) / 100
	
	if count == 1800:
		store=kpa
		count = 0
		curs.execute ('INSERT INTO bmp_sensor_data SET pressure=%s, seapressure=%s,temperature=%s, altitude=%s' % (pressure,seapressure,temp,alt))
		db.commit()

	if kpa > 1060:
		print "EXTREME DRY","\t",kpa,"\t",store
	if kpa > 1030 and kpa < 1060:
		print "DRY","\t",kpa,"\t",store
	if kpa > 1010 and kpa < 1030:
		print "DRY HEADING","\t",kpa,"\t",store
	if kpa > 980 and kpa < 1010:
		print "CHANGE","\t",kpa,"\t",store
	if kpa > 970 and kpa < 980:
		print "RAIN","\t",kpa,"\t",store
	if kpa > 950 and kpa < 970:
		print "STORM","\t",kpa,"\t",store
	if kpa < 950:
		print "EXTREME STORM","\t",kpa,"\t",store
	if humidity is not None and temperature is not None:
		temp = '{0:0.1f}' .format(temperature)
		humid = '{0:0.1f}' .format(humidity)
		temp = float(temp)
		temp = round(temp)
		humid = float(humid)
		humid = round(humid)


	else:
		print('Failed to get reading. Try again!')
	f = open("/mnt/ramdrive/data.log", "w")
	f.write("%s,%s,%s,%s,%s,%s" % (pressure,seapressure,temp,alt,humid,temp))
	f.close()
	# curs.execute ('INSERT INTO bmp_sensor_data SET pressure=%s, seapressure=%s,temperature=%s, altitude=%s' % (pressure,seapressure,temp,alt))
	# db.commit()
	