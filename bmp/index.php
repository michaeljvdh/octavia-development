<style type="text/css">
  .fonttheme {
    color:white;
  }
  html {background-color: #0d0d0d !important;

  }
</style>   
  
  <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../css/uikit.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
        <script src="../js/uikit.min.js"></script>
        <script src="../js/uikit-icons.min.js"></script>
        <script src="../js/jquery.js"></script>
        <!-- <link rel="stylesheet" href="dials/fonts/fonts.css"> -->
        <!-- <script src="dials/gauge.min.js"></script> -->
        <link rel="stylesheet" type="text/css" href="css.css">
        <!-- <script src="//cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.5/all/gauge.min.js"></script> -->
    </head>


<?php
        // include "connection.php";
        // include "nav.php";
        // include "global.php";

?>
<br><br>
     <div class="scoreBoardrelay"></div>
     <!-- <div id="fetch2" class="fetch2"></div> -->
     
     
<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  setInterval( refreshscoreBoardrelay, 1000 );
  var inRequestB = false;
  function refreshscoreBoardrelay() {
    if ( inRequestB ) {
      return false;
    }
    inRequestB = true;
    var load = $.get('fetch.php');
    $(".scoreBoardrelay");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".scoreBoardrelay").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestB = false;
    });
  }
</script>
<!-- ****** PULL STATE CONSTANTLY END ****** -->

<!-- ****** PULL STATE CONSTANTLY ****** -->
<!-- <script type="text/javascript">
  setInterval( refreshfetch2, 1000 );
  var inRequest = false;
  function refreshfetch2() {
    if ( inRequest ) {
      return false;
    }
    inRequest = true;
    var load = $.get('fetch2.php');
    $(".fetch2");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".fetch2").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequest = false;
    });
  }
</script> -->
<!-- ****** PULL STATE CONSTANTLY END ****** -->




            <?php
            function formatDateMysql($timestamp = false) {
                $timestamp = strtotime($timestamp);
                if ($timestamp === false) {
                    $timestamp = time();
                }
                return date('F j, Y, g:i a', $timestamp);
            };

                        function formatDateMysqltime($timestamp = false) {
                $timestamp = strtotime($timestamp);
                if ($timestamp === false) {
                    $timestamp = time();
                }
                return date('g:i a', $timestamp);
            };


            $pressurearray=array();
            $datestamparray=array();
             include "connection.php";
             
             $stmt = $db->query('SELECT * FROM (SELECT * FROM bmp_sensor_data ORDER BY id DESC LIMIT 23) sub ORDER BY id ASC');
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                  
                  $result = $web_pressure - $keep;
                  $keep = $web_pressure;
                  $web_pressure = $row['pressure'] / 100;
                  $result = $web_pressure - $keep;
                  $result = round($result,2);
                  $web_timestamp = $row['stamp'];
                  $web_timestamp = formatDateMysql($web_timestamp);
                  $web_timestamp2 = formatDateMysqltime($web_timestamp);

                  if ($result<0) {$color="red";} else {$color="lightgreen";};
                  array_push($pressurearray, $web_pressure);
                  array_push($datestamparray, '\''.$web_timestamp2.'\'');
                 
              ;};
// print_r($pressurearray);
$test = implode(",", $pressurearray);
$test = ltrim($test,',');

$test2 = implode(",", $datestamparray);
// print $test.'<br>';
// print $test2;

            ?>
        


  <title>Line Chart</title>
  <script src="../Chart.js-master/dist/Chart.min.js"></script>
  <script src="../Chart.js-master/samples/utils.js"></script>
  <style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  </style>

<body>
  <div style="width:100%;">
    <canvas id="canvas"></canvas>

  <script>
    var MONTHS = [<?php print $test2;?>];
    var config = {
      type: 'line',
      data: {
        labels: [<?php print $test2;?>],
        datasets: [{
          label: '12 Hr Pressure',
          backgroundColor: window.chartColors.white,
          borderColor: window.chartColors.blue,
          data: [
            <?php print $test;?>
          ],
          fill: false,
        }
        ]
      },
      options: {
        responsive: true,
        title: {
          display: false,
          text: 'Chart.js Line Chart'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Time'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Pressure Level'
            }
          }]
        }
      }
    };

    window.onload = function() {
      var ctx = document.getElementById('canvas').getContext('2d');
      window.myLine = new Chart(ctx, config);
    };




  </script>

