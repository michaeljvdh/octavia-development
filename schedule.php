
<?php
include "connection.php";
include "header.php";
include "nav.php";
?>


  <link href="app/src/css/scheduler.css" rel="stylesheet" />
  <div class="uk-container">    
    
    
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="app/src/js/scheduler.js"></script>
<div class="uk-container">
  <div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Scheduled GPIO's Weekly Calendar</h3>
<?php
$addpitables = array();
array_push($addpitables, $thisnode);

foreach ($addpitables as $key => $nodes) {
  // print $nodes;
$starttimearray = array();
$starttimeminutesarray = array();
$endtimearray = array();
$endtimeminutesarray = array();
$descriptionarray = array();
$esparray = array();
$dayarray = array();
 
$stmt = $db->query('SELECT * FROM '.$nodes.' ORDER BY objectname,time(beginning),day;');
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                                      $beginining=$row['beginning'];
                                      $ending=$row['ending'];
    
    $id = $row['id'];
    $description = $row['description'];
    
    $begin = strtotime($beginining);
    $end = strtotime($ending);
    $aaa = date("Y-m-d",$begin); 
    $bbb = date("H:i:s",$begin); 
    $beginmod = $aaa." ".$bbb;
    $begintime = $bbb;
    $ccc = date("Y-m-d",$end);
    $ddd = date("H:i:s",$end);
    $endmod = $ccc." ".$ddd;
    $endtime = $ddd;
    $day = $row['day'];
    $test = strstr($day, '8', false);
    if ($day=='8') {$day='0123456';};
    if ($test == "8") { $day="0123456";};
    // print $day;
    $dayarray = str_split($day);


    $starthour = $begintime[0].$begintime[1];
    $startmin = $begintime[3].$begintime[4];
    $endhour = $endtime[0].$endtime[1];
    $endmin = $endtime[3].$endtime[4];

print '<Br>';

if( $endmin >=1  and $endmin <=15) {$addend=1;};
if( $endmin >=16 and $endmin <=30) {$addend=2;};
if( $endmin >=31 and $endmin <=45) {$addend=3;};
if( $endmin >=46 and $endmin <=59) {$addend=4;};
if( $endmin ==00) {$addend=0;};

if( $startmin >=1  and $startmin <=15) {$staddend=1;};
if( $startmin >=16 and $startmin <=30) {$staddend=2;};
if( $startmin >=31 and $startmin <=45) {$staddend=3;};
if( $startmin >=46 and $startmin <=59) {$staddend=4;};
if( $startmin ==00) {$staddend=0;};

// print_r ($dayarray);
print '
<div style="font-size:1em;">Entry Desciption: <a href="schedulegpio.php">'.$description.'</a></div>
    <section>      
      <table id="test'.$id.'"></table>
      <script>
        $(\'#test'.$id.'\').scheduler({
          // multiple: false,          
          footer: false,
          disabled: true,
          accuracy: 4,
          data: {';
            ?>
            <?php 

            foreach ($dayarray as $key => $value) {
              
              $value = $value+1;
             print ''.$value.':[';
                foreach (range($starthour*4+$staddend,$endhour*4-1+$addend) as $number){
                    print $number.',';
                     };# code...
                       print '],';
            };
            ?>
            <?php
            print '
          }
        });
      </script>
      
    </section>
    ';

  };        
};

?>

</div></div></div>