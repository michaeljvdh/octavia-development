
<?php
include "connection.php";
$stmt = $db->query("SELECT * FROM config WHERE description='passcode';");
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $passcode=$row['set1'];
    };




// session_start();
//     $sensorurl = $_SERVER['REQUEST_URI'];
//     if ($sensorurl == "/sensorsdash.php") {;} else {
//     if ($_SESSION['key'] == $passcode) {;}else {print "<meta http-equiv=\"refresh\" content=\"0;URL='login.php'\" />";};
// };



?>

<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@600&family=Turret+Road&display=swap" rel="stylesheet">



<style>
    .menuicon{
        width:20px;
        display: inline-block;
    }
    
  .option:hover {   
    background-color: grey; } /* CSS link hover (green) */

.uk-navbar-nav .option >a:hover {
    color: #FFF;
    outline: 0;
}
</style>
<nav class="uk-navbar-container uk-margin" uk-navbar style="margin-bottom: 2px !important;">
<a class="uk-navbar-item uk-logo" href="index.php"><img src="assets/octavialogo.png" width="50px"></a>
    <div class="uk-navbar">
        <div class="uk-navbar-left"><div>
        <ul class="uk-navbar-nav uk-navbar-toggle">
                    <li>
                        <a href="#" class="uk-active">CONFIGURATION</a>
                        <div class="uk-navbar-dropdown ">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li class="uk-active"><a href="#">SETTINGS</a></li>
                                <li class="option"><a href="addpi.php">&nbsp;- Add/Edit Node</a></li>
                                <li class="option"><a href="thisnode.php">&nbsp;- This Node</a></li>
                                <li class="option"><a href="services.php">&nbsp;- Services</a></li>
                                <li class="option"><a href="logconfig.php">&nbsp;- Logging</a></li>
                                <li class="uk-active"><a href="#">GPIO MANAGEMENT</a></li>
                                <li class="option"><a href="configgpio.php">&nbsp;- Add/Edit Pi GPIO</a></li>
                                <li class="option"><a href="configespgpio.php">&nbsp;- Add/Edit ESP GPIO</a></li>
                                <li class="uk-active"><a href="#">SCHEDULED GPIO's</a></li>
                                <li class="option"><a href="schedulegpio.php">&nbsp;- Configure Schedule</a></li>
                                <li class="uk-active"><a href="#">DYNAMIC RULES</a></li>
                                <li class="option"><a href="rules.php">&nbsp;- Rules</a></li>
                                <li class="uk-active"><a href="#">SENSOR CONFIGURE</a></li>
                                <li class="option"><a href="configds18b20.php">&nbsp;- DS18B20 on Pi</a></li>
                                <li class="option"><a href="configdht1122.php">&nbsp;- DHT1122 on Pi</a></li>
                                <li class="option"><a href="configpca9685.php">&nbsp;- PCA9685 on Pi</a></li>
                                <li class="option"><a href="addbmeesp.php">&nbsp;- Add ESP Endpoint</a></li>
                                <li class="uk-active"><a href="#">NOTIFICATIONS</a></li>
                                <li class="option"><a href="telegramconfig.php">&nbsp;- Telegram API</a></li>
                                <li class="option"><a href="messagecenter.php">&nbsp;- Message Center</a></li>
                                <li class="uk-active"><a href="#">LOGIN</a></li>
                                <li class="option"><a href="loginreset.php">&nbsp;- Reset Login Code</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
    
        </div>
            <div class="uk-navbar"><div>    
                <ul class="uk-navbar-nav uk-navbar-toggle" style="width: 100%;">
                    <li>
                        <a href="#" class="uk-active">USAGE</a>
                        <div class="uk-navbar-dropdown ">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li class="uk-active"><a href="#">REMOTES</a></li>
                                <li class="option"><a href="remote_all_nodes.php">&nbsp;- All nodes</a></li>
                                <li class="option"><a href="remote_single_node.php">&nbsp;- This node</a></li>
                                <li class="option"><a href="sensorsdash.php">&nbsp;- Sensor Dash</a></li>                                
                                <li class="uk-active"><a href="#">VIEWS</a></li>
                                <li class="option"><a href="schedule.php">&nbsp;- Schedule</a></li>
                                <li class="uk-active"><a href="#">CONTROLS</a></li>
                                <li class="option"><a href="pca9685.php">&nbsp;- PCA9685</a></li>
                                <li class="uk-active"><a href="#">INFO</a></li>
                                <li class="option"><a href="diagnostics.php">&nbsp;- DIAGNOSTICS</a></li>
                            </ul>
                        </div>
                    </li>
                    <li style="min-width: 100%;">
                        <a href="login.php" class="uk-active" style="padding:0px;"><strong>LOGOUT</strong></a>                        
                    </li>
                        </ul>
        
            </div>
        
        </div>
    </div>
    </div>
</nav>


<br>
<?php
    $iconsize="style='width:40px;'";
?>
<!-- <div class="" style="z-index: 1000;"> -->

<div class="" style="float:right; margin: 10px;position: absolute;z-index: 1000;top:72;right:0;" >

        <?php

        $onchange= "onChange=\"this.style.background='#fdff8e';\"";
        global $onchange;
        $addpitables = array();
        array_push($addpitables, 'masterrelay');
        $stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
        	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        		$tablename = $row['Tables_in_octavia'];
        		array_push($addpitables, $tablename);
        	;};

        $myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
                $thislocalnode = fgets($myfile);
                $thislocalnode = str_replace('`', '', $thislocalnode);
                fclose($myfile);
                $thislocalnode = trim($thislocalnode);
                $value=$thislocalnode;


        $stmt = $db->query("SELECT * FROM config WHERE description='thisnode' and node='$thislocalnode';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                $set1 = $row['set1'];
                $set2 = $row['set2'];
            };

        // BACKGROUND COLOR !!!
        print "<style type='text/css'>html {background-color:  ".$set2." ;}</style>";
        // BACKGROUND COLOR !!!

        global $thisnode;
        $stmt = $db->query("SELECT * FROM config WHERE description='thisnode'and node='$thislocalnode';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $thisnode=$row['set1'];
            };

        $fromthisurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        ?>

        <?php

        if (basename($_SERVER['REQUEST_URI'])=="thisnode.php") {;} else {

        foreach ($addpitables as $key => $value) {

        	$valuelabel=str_replace("node_", "", $value);
        	$valuelabel=str_replace("_", " ", $valuelabel);
            if ($valuelabel=="masterrelay") {$valuelabel="MASTER Pi";};
            
            if ($set1==$value) {
                $selected='<span style="color:white;background-color:#555;" uk-icon="check"></span>&nbsp;';
                $style="border: solid 1px;border-color:lightgrey;background-color:#ddffd9;";} else {$selected="";$style="";};

            print '
            <div style="display: inline-table;" >
                <form action="submit.php" method="POST" style="margin-bottom: 0px;">
                    <input name="option" value="viewthisnode" hidden>
                    <input name="node" value="'.$value.'" hidden>
                    <input name="fromurl" value='.$fromthisurl.' hidden>
                    <input name="localnode" value='.$thislocalnode.' hidden>
                    <button class= "nodebutton" style="'.$style.'">&nbsp;'.$selected.$valuelabel.'</button>            
                </form>
            </div>
        	';
            };
        };
        ?>

</div>
<!-- </div> -->

<?php
if (isset($_SESSION['saved'])) {

if ($_SESSION["saved"]=="yes") {
    
    
print '<button id="run"  class="demo uk-button uk-button-default save-button" type="button" onclick="UIkit.notification({message: \'CHANGES SAVED\',pos: \'top-right\'})" hidden>Click me</button>';
print "<script>
        
        function myFunctiona(){
            $('#run').click();
        };
        myFunctiona();
        </script>
";
$_SESSION["saved"] = "no";
};

if ($_SESSION["saved"]=="nodeswitch") {
    

    
print '<button id="run"  class="demo uk-button uk-button-default save-button" type="button" onclick="UIkit.notification({message: \'NODE VIEW SWITCHED\',pos: \'top-right\'})" hidden>Click me</button>';
print "<script>
        
        function myFunctiona(){
            $('#run').click();
        };
        myFunctiona();
        </script>
";
$_SESSION["saved"] = "no";
};

if ($_SESSION["saved"]=="telegramtest") {
    
    
print '<button id="run" style="background-color:white !important;" class="demo uk-button uk-button-default save-button" type="button" onclick="UIkit.notification({message: \'TEST MESSAGE SENT\',pos: \'top-right\'})" hidden>Click me</button>';
print "<script>
        
        function myFunctiona(){
            $('#run').click();
        };
        myFunctiona();
        </script>
";
    $_SESSION["saved"] = "no";
    };
};

?>
<br>
<!-- <div class="footer">
  <div style="color: black;">BETA - <a href="http://www.piworx.net">www.piworx.net</a></div>
</div> -->






    <!-- <h1 style="position:absolute;z-index: -1;left:100px:top150px;">NODE</h1> -->
<style type="text/css">
    .clock {
    position: absolute;
    top: 40px;
    right: -30px;
    transform: translateX(-50%) translateY(-50%);
    color: #fff;
    font-size: 12px;
    font-family: Orbitron;
    letter-spacing: 2px;
    background-color: #555;
    border-radius: 5px;
    padding: 5px;

}
</style>
<div id="MyClockDisplay" class="clock" onload="showTime()"></div>

    <script type="text/javascript">
        function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();
    </script>