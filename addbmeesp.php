
<?php

include "connection.php";
include "header.php";
include "nav.php";


$onchange = "";

?>

<!-- HELP -->
<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Adding an ESP Endpoint</h2>
        <h4>Description</h4>
        <p>Help to be determined.</p>

    </div>
</div>
<!-- HELP -->

<style type="text/css">
    .stylemyinput_checkbox {
    min-height: 30px;
    min-width: 30px;        
    }
    th {
        text-align: center;
    }
 :focus {outline: none !important;}
 td {
    margin: 10px !important;
 }
</style>

<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body">
    <div>
        <div style="display: inline-table;"><h3 class="uk-card-title">Adding an ESP Endpoint</h3></div>
        <div style="display: inline-table;float: right;"><a class="" href="#modal-container" uk-toggle><span uk-icon="icon: question;"></span></a></div>
    </div>

<!-- Accordian open -->

<!-- Data -->
<div class="<?php print $theme;?>"  >
    <form class="" name = "" action="submit.php" method="POST" style="margin:10px;padding-bottom: 10px;">
        <table class="uk-table " id="tbl_posts" >
                <input name="option" value = "addbmeesp" hidden>
                <input name="thisnode" value="<?php print $thisnode; ?>" hidden>
                <tr>
                    <thead>
                            <th>Description</th>                            
                            <th>ip / dns</th>                            
                            <th>Delete</th>
                    </thead>
                </tr>
            <tbody id="tbl_posts_body">
            <tr id="rec-1">
<!-- REPLICATION PORTION -->
 <?php
 $stmt = $db->query("SELECT * FROM bme WHERE node='$thisnode';");
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    	$ide = $row['id'];
    	$description = $row['description'];
    	$ipdns = $row['ipaddr'];

    	print '
    	<tr><td><input name="ide[]" value="'.$ide.'"hidden><input name="node" value="'.$thisnode.'"hidden><input type="text" class="uk-input" name="description[]" value="'.$description.'"></td>
		<td><input  class="uk-input" name="ipdns[]" value="'.$ipdns.'"></td>
		<td><input name="del[]" class="uk-checkbox" type="checkbox" value='.$ide.'></td>
		</tr>
    	';
    };

    	?>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3"><hr></td></tr>
<!-- <td><input type="text" class="uk-input" name="description[]" ></td>
<td><input class="uk-input" name="ipdns[]"></td> -->

<!-- REPLICATION PORTION -->
                    
                    <td></td><td></td>  <td  style="text-align: left;"><a class="noselect add-record uk-button uk-button-default save-button" data-added="0">Add Row(s)</a></td>

                </tr>
            </tbody>
            
            </table>
            <input type="text" value="master" name="fromurl" hidden>
            <button class="<?php print $theme;?> uk-button uk-button-default save-button" type="submit">Update / Save</button>
        </form>


</div>
       
       <br>

<div style="display:none;">
    
    <table class="uk-table" id="sample_table">
                        
          
      <tr id="">
<!-- REPLICATION PORTION -->

<td><input name="ide[]" value="new" hidden><input type="text" class="uk-input" name="description[]" required></td>
<td><input  class="uk-input" name="ipdns[]" required></td>

<!-- REPLICATION PORTION -->
                <td  style="text-align: left;vertical-align: middle;padding-left:20px;"><a class="btn btn-xs delete-record" data-id="0"><i class="uk-icon-link" uk-icon="trash"  style="color: red;"></a></td>

     </tr>     
   </table>
 </div>

</div>
<script type="text/javascript">
    jQuery(document).delegate('a.add-record', 'click', function(e) {
        e.preventDefault();    
        var content = jQuery('#sample_table tr'),
        size = jQuery('#tbl_posts >tbody >tr').length + 1,
        element = null,    
        element = content.clone();
        element.attr('id', 'rec-'+size);
        element.find('.delete-record').attr('data-id', size);
        element.appendTo('#tbl_posts_body');
        element.find('.sn').html(size);
    });

    jQuery(document).delegate('a.delete-record', 'click', function(e) {
        e.preventDefault();    
        // var didConfirm = confirm("Are you sure You want to delete");
        // if (didConfirm == true) {
            if (1 == 1) {
        var id = jQuery(this).attr('data-id');
        var targetDiv = jQuery(this).attr('targetDiv');
        jQuery('#rec-' + id).remove();
        
        //regnerate index number on table
        $('#tbl_posts_body tr').each(function(index) {
        //alert(index);
        $(this).find('span.sn').html(index+1);
        });
        return true;
    } else {
        return false;
    }
    });
</script>
</div>