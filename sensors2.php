

<style>
    .sensorblock {
        /*display: block;*/
        background-color: none;        
        /*max-width: 200px;*/\
    }
    

    .uk-button {
        border:none;
    }

    .uk-modal-body {
        padding: 10px;
        width: 650px !important;
    }
    uk-modal-dialog uk-modal-body


.uk-table, th, td {
    text-align: center !important;    
    
}

.uk-table th {font-size: 0.8em;}

</style>




<?php


include "connection.php";
// include "header.php";

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
$thisnode = fgets($myfile);
$thisnode = str_replace('`', '', $thisnode);
fclose($myfile);
$thisnode = trim($thisnode);
$thislocalnode = $thisnode;
$thislocalnode = str_replace('masterrelay', 'Master Pi', $thislocalnode);


print '<div class="uk-container">';

print '<div class="sensorblock" style="">';
            $serials=array();
            $idarray = array();
            $description=array();
            $stmt = $db->query("select * from ds18b20 where node='$thisnode';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                array_push($serials, $row['serialnumber']);
                array_push($description, $row['description']);
                $sensorsavail_ds18b20="yes";
                $id = $row['id'];
                array_push($idarray, $id);
            };
print '     
            
            
            ';
                        if (isset($sensorsavail_ds18b20)) {;}else {Print "No Sensors Found or Configured !";};

                       
print '<table class="uk-table uk-table-small uk-table-middle uk-table-divider" style="" border="1">
        <th>Ds18b20</th><th>Location</th><th>Humidity</th><th>Temperature</th><th>Graph</th>';
            foreach ($serials as $key => $value) {
            $unique=mt_rand();
             print '
                    
                        <div id="modal-example'.$unique.'" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body" align="center">
                                <h2 class="uk-modal-title">Ds18b20 Pi '.$idarray[$key].'</h2>
       
                                    <script>
                                        $(document).ready(function(){    
                                            $("#box'.$unique.'").load("apex.php?table=ds18b20&attr=temperature&table_id='.$idarray[$key].'&displaytype=ds18b20");               
                                            });
                                    </script>
                                
                                    <div id="box'.$unique.'"></div> 

                                    <p class="uk-text-right">
                                        <button class="uk-button uk-button-default uk-modal-close" type="button">Close</button>            
                                    </p>
    
                            </div>
                        </div>



    ';
///////////////////////
    if (file_exists("/mnt/octavia/".$value.".dat")) {
    
        $myfile = fopen("/mnt/octavia/".$value.".dat", "r") or die("Unable to open file!");
        $temperature = fgets($myfile);
        fclose($myfile);
        } else {$temperature='<div style="color:red; font-size:0.9em">No Sensor Found</div>';};

    print '            
                    <tr>
                    <td><span uk-icon="triangle-right"  style="width:40px;height:40px;text-align:center;"></td>
                    <td>'.ucfirst($description[$key]).'</td>
                        
                        <td>Temperature</td>
                        <td><div class="sensorreadings">'.$temperature.'</div>
                        </td>
                        <td><button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-example'.$unique.'">Graph</button></td>
                    
                    </tr>';
        

        
       
        };
print '</table>
</div>
        
        
        <hr>';

////////////////////////



////////////////////////
print '<div class="sensorblock" style="">';
$dhtgpios=array();
$idarray = array();
$dhtdescriptions=array();
$stmt = $db->query("select * from dht1122 where node='$thisnode';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($dhtgpios, $row['gpio']);
        array_push($dhtdescriptions, $row['description']);
        $sensorsavail_dht="yes";
        array_push($idarray, $row['id']);
    };   


print '


    <div class="" style="margin-left:0px;">
        <div style="margin:0 auto;">
';
if (isset($sensorsavail_dht)) {;}else {Print "No Sensors Found or Configured !";};



print '<table class="uk-table uk-table-small uk-table-middle uk-table-divider" style="" border="1">
        <th>DHT</th><th>Location</th><th>Humidity</th><th>Temperature</th><th>Graph</th>';
foreach ($dhtgpios as $key => $value) {

    ////////
    $unique=mt_rand();;

    print '
    ';
    print '
 
                            <!-- This is the modal -->
                            <div id="modal-example'.$unique.'" uk-modal>
                                <div class="uk-modal-dialog uk-modal-body" align="center">
                                    <h2 class="uk-modal-title">DHT1122 Pi :  '.$idarray[$key].'</h2>

                                   
                            <script>
                            $(document).ready(function(){    
                                    $("#box'.$unique.'").load("apex.php?table=dht1122&attr=temperature&table_id='.$idarray[$key].'&displaytype=dht1122");               
                                    $("#boxhumidity'.$unique.'").load("apex.php?table=dht1122&attr=humidity&table_id='.$idarray[$key].'&displaytype=dht1122");
                                });

                            </script>

                                <div id="box'.$unique.'"></div> 
                                <div id="boxhumidity'.$unique.'"></div> 



                                    <p class="uk-text-right">
                                        <button class="uk-button uk-button-default uk-modal-close" type="button">Close</button>
                                        
                                    </p>
                                </div>
                            </div>


    ';
///////////////////////


    if (file_exists("/mnt/octavia/".$value.".dat")) {
    $myfile = fopen("/mnt/octavia/".$value.".dat", "r") or die("Unable to open file!");
    $humidity = fgets($myfile);
    fclose($myfile);
    $myfile = fopen("/mnt/octavia/".$value."t.dat", "r") or die("Unable to open file!");
    $temperature = fgets($myfile);
    fclose($myfile);
        }else {$temperature='<div style="color:red;">No Sensor Err#1</div>';};
    print '
    
    
      
                
             <tr>
             <td><span uk-icon="triangle-right"  style="width:40px;height:40px;text-align:center;"></td>
             <td>'.ucfirst($dhtdescriptions[$key]).'</td>
                    
                    <td><div class="sensorreadings">'.$humidity.'</div>
                    </td>
             
             
                
                     <td><div class="sensorreadings">'.$temperature.'</div>
                     </td>
                    <td><button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-example'.$unique.'">Graph</button></td>
             </tr>
          
    
    ';
print '</div></div></div></div>';    
};

print '</table>';



print '</div>';
print '</div></div><hr>';

////////////////////////


////////////////////////
print '<div class="sensorblock" style="">';

$idarray = array();
$stmt = $db->query("SELECT * FROM bme WHERE node='$thisnode';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
array_push($idarray, $row['id']); 
};

$flip = 0;

$stmt = $db->query("SELECT * FROM bme WHERE node='$thisnode';");
    
print '<table class="uk-table uk-table-small uk-table-middle uk-table-divider" style="" border="1">
<th>BME</th><th>Location</th><th>Temperature</th><th>Humidity</th><th>Pressure</th><th>Altitude</th><th>Graph</th>';
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $bmeipaddr = $row['ipaddr'];
        $description = $row['description'];
        $data = file_get_contents('/mnt/octavia/'.$bmeipaddr.'.json');
        $characters = json_decode($data); // decode the JSON feed
   

foreach ($characters as $key =>$character) {
    

    if(is_null($character)){;} else {    
        if(property_exists($character, "temperature")) {


    ////////
    $unique=mt_rand();
    print '
    
    
                                <!-- This is the modal -->
                                <div id="modal-example'.$unique.'" uk-modal>
                                    <div class="uk-modal-dialog uk-modal-body" align="center">
                                        <h2 class="uk-modal-title">Bme280E ESP : '.$idarray[$flip].'</h2>

                                       
                                <script>
                                $(document).ready(function(){    
                                        $("#box'.$unique.'").load("apex.php?table=bme&attr=temperature&table_id='.$idarray[$flip].'&displaytype=bme");               
                                        $("#boxhumidity'.$unique.'").load("apex.php?table=bme&attr=humidity&table_id='.$idarray[$flip].'&displaytype=bme");
                                        $("#boxpressure'.$unique.'").load("apex.php?table=bme&attr=pressure&table_id='.$idarray[$flip].'&displaytype=bme");
                                    });

                                </script>

                                    <div id="box'.$unique.'"></div> 
                                    <div id="boxhumidity'.$unique.'"></div> 
                                    <div id="boxpressure'.$unique.'"></div> 



                                        <p class="uk-text-right">
                                            <button class="uk-button uk-button-default uk-modal-close" type="button">Close</button>
                                            
                                        </p>
                                    </div>
                                </div>';

                print '            
 
        <tr>
            <td><span uk-icon="triangle-right"  style="width:40px;height:40px;text-align:center;"></span></td>
            <td>'.$description.'</td>
                        
            <td>
                <div class="sensorreadings">'.$character->temperature.'</div>
            </td>
            <td>
                <div class="sensorreadings">'.$character->humidity.'</div>
            </td>  
            <td>
                <div class="sensorreadings">'.$character->pressure.'</div>
            </td>
            <td>
                <div class="sensorreadings">'.$character->altitude.'</div>
            </td>
            <td>    <button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-example'.$unique.'">Graph</button></td>
        </tr>';
$flip = $flip +1;

                };
            };
        };
    };
print '</table>';
print '</div>';
print '<br><br>';

print '
<div align="center">
    <div class="uk-card uk-card-default uk-card-body" style="max-width:90%;">
        <p>Note that these sensors are local to '.$thislocalnode.' only.  And are not impacted by the node view switcher on the top right.  If you wish to view sensors on another Pi, please link it to it\'s web address.</p>
    </div>
</div>';
print '</div>';

?>

<!-- This is a button toggling the modal -->
