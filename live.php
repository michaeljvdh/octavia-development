<!-- <link rel="stylesheet" href="css/uikit.min.css" /> -->
<link rel="stylesheet" href="css/<?php print $theme; ?>.css" />


<?php
include "connection.php";
$stmt = $db->query("SELECT * FROM config WHERE description='logo';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $logo=$row['set1'];
                $theme=$row['set2'];
            };


// include "connection.php";
// $stmt = $db->query("SELECT * FROM config WHERE description='logo';");
//             while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//                 $logo=$row['set1'];
//                 $theme=$row['set2'];
//             };


// print "Hello".$theme;
if ($theme=="uk-style-dark"){
  $labelcolor = "white";
} else {$labelcolor="black";};




// include "header.php";
$description=$_GET['description'];
$type=$_GET['type'];
if (isset($_GET['color'])) {$color=$_GET['color'];};


$db = new SQLite3('/mnt/octavia/test.db');
$db->busyTimeout(2000);
$stmt = $db->query("SELECT * FROM sensors WHERE description='$description';");
while ($row = $stmt->fetchArray()) {
    $id=$row['id'];
    $description=$row['description'];
    $temperature=$row['temperature'];
    $humidity=$row['humidity'];
    $pressure=$row['pressure'];
    $altitude=$row['altitude'];
    $time=date("h:i:sa"); 
};


if ($type=="temperature") 	{$min=-10;$max=35;$initial=$temperature;};
if ($type=="humidity") 		{$min=0;$max=100;$initial=$humidity;};
if ($type=="pressure")		{$min=920;$max=1020;$initial=$pressure;};
if ($type=="altitude") 		{$min=0;$max=1;$initial=$altitude;};




?>

    <meta name="viewport" content="width=device-width">
    <style> 
    

      .wrapper {
   /*   position: relative;
      width: 640px;
      height: 480px;
      margin: 50px auto 0 auto;
      padding-bottom: 300px;
      border: 1px solid #ccc;
      border-radius: 3px;
      clear: both;*/
    }

    .box {
      float: left;
      width: 50%;
      height: 50%;
      box-sizing: border-box;
    }

    .container {
      width: 450px;
      margin: 0 auto;
      text-align: center;
    }

    .gauge {
    	position: relative;
    	top:0px;
      width: 400px;
      height: 200px;
      padding: 0px;
      /*border:solid 1px;*/
    }
    
   

    svg {    	
    	margin: 0px !important;
    	padding: 0px !important;
    	/*background-color: red;*/
    }

    </style>
  

  <div align="center">
    	<!-- <div class="box"> -->
        
          <div id="guage1" class="guage" style="background-color: <?php print $color;?>";></div>    
        
  </div>
    <!-- </div> -->
    <script src="/try/justgage-1.2.2/raphael-2.1.4.min.js"></script>
    <script src="/try/justgage-1.2.2/justgage.js"></script>
    <script>
    document.addEventListener("DOMContentLoaded", function(event) {
      


      var guage1 = new JustGage({
        id: 'guage1',
        value: <?php print $initial;?>,
        min: <?php print $min;?>,
        max: <?php print $max;?>,
        decimals: 2,
        valueFontColor: "<?php print $labelcolor;?>",
        symbol: '',
        pointer: true,
        gaugeWidthScale: 1.1,
        relativeGaugeSize: true,
        startAnimationTime: 1,
        startAnimationType: 'bounce',
        refreshAnimationTime: 600,        
        refreshAnimationType: 'bounce',
        relativeGaugeSize: false, //Whether gauge size should follow changes in container element size
        title: '<?php print $description; ?>',

        customSectors: [{
          color: '#ff0000',
          lo: 10,
          hi: 35
        }, {
          color: '#00ff00',
          lo: -10,
          hi: 10
        }],
        counter: true        
      });

var x=<?php print $initial;?>;

setInterval(function repeat(){
		function loadDoc() {  
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      document.getElementById("demo").innerHTML =
		      this.responseText;
		      x = this.responseText;      
		    }
		  };
		  var getthis = "livedata.php?name=<?php print $description; ?>&type=<?php print $type; ?>";
      xhttp.open("GET", getthis, true);
		  xhttp.send();
		};

		loadDoc();
		guage1.refresh(x);	

		},2000);


    });

    </script>


<div id="demo" hidden ></div>



