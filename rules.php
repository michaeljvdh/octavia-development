
<?php
include "connection.php";
include "header.php";
include "nav.php";

$ds18b20_id_array = array();    
$gpio_id_array = array();
$esp_id_array = array();
$dht1122_id_array = array();



        $stmt = $db->query("SELECT id FROM ds18b20 WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("ds18b20,".$row['id']."");
            array_push($ds18b20_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM dht1122 WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("dht1122,".$row['id']."");
            array_push($dht1122_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM gpio WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("gpio,".$row['id']."");
            array_push($gpio_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM esp WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("esp,".$row['id']."");
            array_push($esp_id_array, $id);
        };
// print $thisnode;
?>

<style type="text/css">
    .inputsizelimit {
        /*width: 60px !important;*/
        /*background-color: red;*/

    }
    
</style>



<!-- HELP -->
<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Dynamic Rules</h2>
        <h4>Validator</h4>
        <p>The "Validator" section works like this: The Validator tests to see if a situation is True or False, the Validator has four validations available.  Tip: Remember to hit REFRESH to be certain of the test outcome.  This will become a dynamic feature in the next version.
        </p>
        <ul>
            <li><strong>Between Values:</strong>
                <p>Classically used for sensors that provide outputs with a variety of conditions that determine the value.</p>
            </li>
            <li><strong>Between Times:</strong>
                <p>Literaly IF the time is currently between time A and time B, B stops at 11:59PM</p>
                <p>Multiple time tests can be put in place, the reason we stop at 11:59PM is the complication of "next day" which would require a calendar and a range of other complications that defeats the daily/weekly purpose.</p>
            </li>
            <li><strong>Is Equal To:</strong>
                <p>Literally equal to an exact number.</p>
            </li>
            <li><strong>GPIO is:</strong>
                <p>GPIO's for meant for checking the Raspberry Pi and or ESP pins status, and are determined with the logic of 0 = on and 1 = off.  If this is chosen use these numbers to indicate your test.</p>
            </li>
        </ul>
        <h4>Initiator</h4>
        <p>The Initiator is where you can stipulate "DO SOMETHING", based on what the test result is in the Validator.  You can easily see the benefit of this, as you can have multiple actions on something that transpired, not just 1.</p>
        <p>Essentially:
        <ul>
            <li><strong>IF RULE:</strong>
                <p>Select the corresponding Validator rule your are referring to.</p>
            </li>
            <li><strong>IS:</strong>
                <p>Meaning what the FACT result was in the Validator</p>
            </li>
            <li>
                <strong>THEN SET:</strong>
                <p>Choose a GPIO/ESP GPIO or a PCA9685(PWM) Channel to set.</p>
            </li>
            <li>
                <strong>TO THIS % or #L:</strong>
                <p>Essentially right now there are only 3 possible entries. 0-100 for PWM as a percentage, 0 for Off (GPIO/ESP) and 1 for On (GPIO/ESP).</p>
            </li>
            <li><strong>RULE ACTIVE:</strong>
                <p>By Default a rule is disabled, you can use this to park rules if you like.</p>
            </li>
            <li><strong>ALERT:</strong>
                <p>Send an alert IF the change matches your stipulation.  The Alert is send to your configured Telegram Bot.</p>
            </li>
            <li><strong>MAX ALERT TODAY:</strong>
                <p>Alerts can pivot, meaning if you enable alerts for a temparature change, there is a possibility if it's room temperature ... to hover and be true/false/true/false ... this threshold allows you to quit after X notifications.  A throttle control effectively.</p>
            </li>


        </ul>

        </p>

        <p></p>
    </div>
</div>
<!-- HELP -->
<br>


<div class="uk-container">

<form action="submit.php" method="POST" id="thisform">
    <input name="option" value="ifttt" hidden>
    <input name="node" value="<?php print $thisnode;?>" hidden>
<div class="container" style="min-width: 800px;">

<div class="uk-card uk-card-default uk-card-body">
    <div>
        <div style="display: inline-table;"><h3 class="uk-card-title">Rule Manager</h3></div>
        <div style="display: inline-table;float: right;"><a class="" href="#modal-container" uk-toggle><span uk-icon="icon: question;"></span></a></div>
    </div>
    <hr style="margin-top:10px;">

   
   <!-- ---------------------------------------- -->
   <div style="margin:0px;">
    <div style="display: inline-table;">
        <h3>Validator Time</h3>
    </div>
    <div style="display: inline-table; float:right;">
        <div class="uk-button uk-button-default save-button" onclick="window.location.href ='addtryrule.php'">Add an Action rule</div>
    </div>
</div>




<div style="">

<div style="display: inline-block;max-width: 90%">
<table class=" " style="">
    <thead>
        
        <th>#</th>
        <th>Sensor</th>
        <th>Description</th>
        <th>Rule Type</th>
        <th>A</th>
        <th>B</th>
        
        <th>DEL</th>
        
    </thead>
    <tbody>
    <!-- <span uk-spinner="ratio: 4.5"></span> -->
<?php
$stmt = $db->query("SELECT * from iffy2  WHERE node='$thisnode' AND objectname='time,0';");
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $id = $row['id'];
        $rulenumber = $row['ruletype'];
        $description = $row['description'];
        $ruletype = $row['ruletype'];
        $objectname = $row['objectname'];
        $a = $row['A'];
        $b = $row['B'];
        // print $b;
        // if ($b==NULL){print "empty";};
        $c = $row['C'];
        if ($c==1) {$fact="True";$factcolor="color:green;font-weight:bold;";} else {$fact="False";$factcolor="color:red;font-weight:bold;";};        

        if ($ruletype==3 or $ruletype==2) {$disable_b='readonly="readonly" hidden'; $b="0";} else {$disable_b="";};

        if ($ruletype == 0) {$r0="selected";$r1="";$r2="";$r3="";$type='number';$step='';};
                if ($ruletype == 1) {$r0="";$r1="selected";$r2="";$r3="";$type='time';$step='step="1"';};
                if ($ruletype == 2) {$r0="";$r1="";$r2="selected";$r3="";$type='number';$step='';};
                if ($ruletype == 3) {$r0="";$r1="";$r2="";$r3="selected";$type='number';$step='';};
        
        // print $b;
        print '<tr>';
        print '
               <td style="max-width:60px;"><input name="iftttId[]" class="uk-input" value="'.$id.'" ></td>
               
               <td>';               
             
                    print '<input value="time,0" name="iftttObjectname[]" hidden>Time';

            print '</select></td>';
            // $stmt5 = $db->query("SELECT * from iffy2 WHERE  id='$id';");
            // while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) {
                // $ruletype = $row5['ruletype'];
                
                    // };    
            print '



               <td><input name="iftttDescription[]" class="uk-input" value="'.$description.'"></td>
               <td>
                    
                    
                        <input value="1" name="iftttRuletype[]" hidden>Between
                                           
                    
               
               </td>
               <td><input name="iftttA[]" class="uk-input" value="'.$a.'" type="'.$type.'" id="" '.$step.' ></td>
               <td><input name="iftttB[]" class="uk-input" value="'.$b.'" '.$disable_b.' type="'.$type.'" '.$step.'  ></td>
               <input name="iftttC[]" class="uk-input" value="'.$fact.'" style="'.$factcolor.'" disabled hidden>
               <td style=""><input  class="uk-checkbox delete-checkbox-color"  type="checkbox" name="iftttremove[]" value="'.$id.','.$thisnode.'"></td>
            
               ';
               
        print '</tr>







        ';
        $disable_b="";
        
    };
?>


    </tbody>
</table>
</div>
   <div style="display: inline-block;">
        <div class="livedata3"></div>
    </div>

</div>
   <!-- ------------------------------------------------- -->


</div>
<br>
<div class="uk-card uk-card-default uk-card-body">

<div style="margin:0px;">
    <div style="display: inline-table;">
        <h3>Validator Local Pi GPIO & Remote ESP GPIO</h3>
    </div>
    <!-- <div style="display: inline-table; float:right;">
        <div class="uk-button uk-button-default save-button" onclick="window.location.href ='addtryrule.php'">Add an Action rule</div>
    </div> -->
</div>




<div style="">

<div style="display: inline-block;max-width: 90%">
<table class=" " style="">
    <thead>
        
        <th>#</th>
        <th>Sensor</th>
        <th>Description</th>
        <th>Rule Type</th>
        <th>A</th>
        <th>B</th>
        
        <th>DEL</th>
        
    </thead>
    <tbody>
    <!-- <span uk-spinner="ratio: 4.5"></span> -->
<?php
$stmt = $db->query("SELECT * from iffy2  WHERE node='$thisnode' AND ruletype !=1;");
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $id = $row['id'];
        $rulenumber = $row['ruletype'];
        $description = $row['description'];
        $ruletype = $row['ruletype'];
        $objectname = $row['objectname'];
        $a = $row['A'];
        $b = $row['B'];
        // print $b;
        // if ($b==NULL){print "empty";};
        $c = $row['C'];
        if ($c==1) {$fact="True";$factcolor="color:green;font-weight:bold;";} else {$fact="False";$factcolor="color:red;font-weight:bold;";};        

        if ($ruletype==3 or $ruletype==2) {$disable_b='readonly="readonly" hidden'; $b="0";} else {$disable_b="";};

        if ($ruletype == 0) {$r0="selected";$r1="";$r2="";$r3="";$type='number';$step='';};
                if ($ruletype == 1) {$r0="";$r1="selected";$r2="";$r3="";$type='time';$step='step="1"';};
                if ($ruletype == 2) {$r0="";$r1="";$r2="selected";$r3="";$type='number';$step='';};
                if ($ruletype == 3) {$r0="";$r1="";$r2="";$r3="selected";$type='number';$step='';};
        
        // print $b;
        print '<tr>';
        print '
               <td style="max-width:60px;"><input name="iftttId[]" class="uk-input" value="'.$id.'" ></td>
               
               <td><select class="uk-select" name="iftttObjectname[]" ">';
                
               foreach($dht1122_id_array as $key => $value) {
                    $x = explode(",",$value);
                        if ($objectname == $value){$selected = "selected";}else{$selected="";};
                            $stmt2 = $db->query("SELECT * from dht1122 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $dhtname=$row2['description'];                                    
                                            $dhtDescription=$row2['description'];
                                    };                    
                    print '<option value="'.$value.'" '.$selected.'>DHT: ('.$dhtname.')</option>';
                };
                
               foreach($ds18b20_id_array as $key => $value) {
                    $x = explode(",",$value);
                        if ($objectname == $value){$selected = "selected";}else{$selected="";};
                            $stmt2 = $db->query("SELECT * from ds18b20 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $ds18b20name=$row2['description'];
                                            $ds18b20Description=$row2['description'];                                    
                                    };                  
                    print '<option value="'.$value.'" '.$selected.'>DS18B20: ('.$ds18b20name.')</option>';
                };

                foreach($gpio_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    if ($objectname == $value){$selected = "selected";}else{$selected="";};
                        $stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $gpioNumber=$row2['number'];
                                        $gpioDescription=$row2['description'];                                    
                                };               
                    print '<option value="'.$value.'" '.$selected.'>GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
                        };

                foreach($esp_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    if ($objectname == $value){$selected = "selected";}else{$selected="";};
                        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $espNumber=$row2['number'];
                                        $espDescription=$row2['description'];                                    
                                };               
                    
                    print '<option value="'.$value.'" '.$selected.'>ESP: '.$espNumber.' ('.$espDescription.')</option>';
                        };
                    print '<option value="time,0" '.$r1.'>Time</optoin>';

            print '</select></td>';
            // $stmt5 = $db->query("SELECT * from iffy2 WHERE  id='$id';");
            // while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) {
                // $ruletype = $row5['ruletype'];
                
                    // };    
            print '



               <td><input name="iftttDescription[]" class="uk-input" value="'.$description.'"></td>
               <td>
                    
                    <select  class="uk-select" name="iftttRuletype[]" id="" onchange="myFunction()" >
                        <option value="0" '.$r0.'>Between Values</option>
                        
                        <option value="2" '.$r2.'>Is Equal To</option>                        
                        <option value="3" '.$r3.'>GPIO is</option>                        
                    </select>
               
               </td>
               <td><input name="iftttA[]" class="uk-input" value="'.$a.'" type="'.$type.'" id="" '.$step.' ></td>
               <td><input name="iftttB[]" class="uk-input" value="'.$b.'" '.$disable_b.' type="'.$type.'" '.$step.'  ></td>
               <input name="iftttC[]" class="uk-input" value="'.$fact.'" style="'.$factcolor.'" disabled hidden>
               <td style=""><input  class="uk-checkbox delete-checkbox-color"  type="checkbox" name="iftttremove[]" value="'.$id.','.$thisnode.'"></td>
            
               ';
               
        print '</tr>







        ';
        $disable_b="";
        
    };
?>


    </tbody>
</table>
</div>

    <div style="display: inline-block;">
        <div class="livedata1"></div>
    </div>

</div>



<!-- '.$disable_b.' -->

</div>
<br>
<div class="uk-card uk-card-default uk-card-body">




<!-- ---------------------------- -->

<info id="gotoesprules">
    <div style="display: inline-table; float:right;">
        <button type="submit" form="addespruleform" class="uk-button uk-button-default save-button" >Add ESP SENSOR Validator Line</button>
    </div>

<h3>Validator ESP Endpoint Sensors</h3>



<div style="">
<div style="display: inline-block;max-width: 90%">
<table class="">
    <thead>
    <th>ESP(#)</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th>DEL</th>
    </thead>

<?php

$stmtmaster = $db->query("SELECT * from validator WHERE node='masterrelay';");
            while($rowmaster = $stmtmaster->fetch(PDO::FETCH_ASSOC)) {
                $id = $rowmaster['id'];
                $tablename = $rowmaster['tablename'];
                $table_id = $rowmaster['table_id'];
                $devicetype = $rowmaster['devicetype'];
                $sensorattribute = $rowmaster['sensorattribute'];
                $ruletypev = $rowmaster['ruletype'];
                $a = $rowmaster['A'];
                $b = $rowmaster['B'];                                
                $c = $rowmaster['C'];
                if ($c==1) {$c="True";$factcolor="color:green;font-weight:bold;";} else {$c="False";$factcolor="color:red;font-weight:bold;";};   
                $node = $rowmaster['node'];
                
                $selectedds18b20="";
                $selecteddht11220="";
                $selecteddht11221="";
                $selectedbme0="";
                $selectedbme1="";
                $selectedbme2="";
                $selectedbme3="";

                // $equal="";
                // $more="";
                // $less="";
                // $between="";
                // $noespruleselected="";

                if ($devicetype=="ds18b20" and $sensorattribute==0) {$selectedds18b20="selected";};

                if ($devicetype=="dht1122" and $sensorattribute==0) {$selecteddht11220="selected";};
                if ($devicetype=="dht1122" and $sensorattribute==1) {$selecteddht11221="selected";};
              

                if ($devicetype=="bme280" and $sensorattribute==0) {$selectedbme0="selected";};
                if ($devicetype=="bme280" and $sensorattribute==1) {$selectedbme1="selected";}; 
                if ($devicetype=="bme280" and $sensorattribute==2) {$selectedbme2="selected";};
                if ($devicetype=="bme280" and $sensorattribute==3) {$selectedbme3="selected";};
                
                
                if (is_null($ruletypev)) {$noespruleselected="selected";$equal="";$more="";$less="";$between="";};
                if ($ruletypev==0) {$equal="selected";$more="";$less="";$between="";$noespruleselected="";};
                if ($ruletypev==1) {$less="selected";$equal="";$more="";$between="";$noespruleselected="";};
                if ($ruletypev==2) {$more="selected";$equal="";$less="";$between="";$noespruleselected="";};
                if ($ruletypev==3) {$between="selected";$equal="";$more="";$less="";$noespruleselected="";};
                
                                

        print '
<tr>
    <td><input value="'.$id.'" class="uk-input"  name="espid[]"></td>
    <td>
        <select name="espunits[]" class="uk-select">
            <option>Select a ESP Endpoint</option>
            ';
                $stmt = $db->query("SELECT * from bme WHERE node='masterrelay';");
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $bme_id = $row['id'];
                    if ($bme_id==$table_id) {$bmeselected = "selected";} else {$bmeselected="";};
                    $description = $row['description'];
                    print '<option value="'.$bme_id.'" '.$bmeselected.'>'.$description.'</option>';

                    };
                print '
        </select>
    </td>
    <td>
        <select id="Sensor'.$id.'" class="uk-select" name="espsensortype[]" >
        <option>Select a Sensor</option>
        ';
        
         $stmt = $db->query("SELECT * from devicetype WHERE code=10;");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $device = $row['description'];
                if ($devicetype==$device) {$selected = "selected";} else {$selected="";};
                
                print '<option value="'.$device.'" '.$selected.'>'.$device.'</option>';

                };
        
        print'
        </select>
    </td>
    <td>
        <select id="sensorattr'.$id.'" class="uk-select" name="espattribute[]">
        <option value="0"     class="ds18b20" '.$selectedds18b20.'>Temperature</option>
        <option value="0"     class="dht1122" '.$selecteddht11220.'>Temperature</option>
        <option value="1"        class="dht1122" '.$selecteddht11221.'>Humidty</option>
        <option value="0"     class="bme280"  '.$selectedbme0.'>Temperature</option>
        <option value="1"        class="bme280"  '.$selectedbme1.'>Humidty</option>
        <option value="2"        class="bme280"  '.$selectedbme2.'>Pressure</option>
        <option value="3"        class="bme280"  '.$selectedbme3.'>Altitude</option>
        </select>
    </td>

   <td>
        <select id="test'.$id.'" class="uk-select" name="esprule[]">
        <option value=""  '.$noespruleselected.'>Select a test</option>
        <option value="0" '.$equal.'>equal to</option>
        <option value="1" '.$less.'>less than</option>
        <option value="2" '.$more.'>greater than</option>
        <option value="3" '.$between.'>between</option>        
        </select>
        
    </td>
    <td>
        <input value="'.$a.'" id="a'.$id.'" class="uk-input" name="espa[]">
    </td>
    <td>
       <div id="hidden_div'.$id.'" ><input value="'.$b.'" id="b'.$id.'" class="uk-input" name="espb[]"></div>
    </td>
<input class="uk-input " disabled value="'.$c.'" style="'.$factcolor.'"" hidden>
<td style="width:20px !important;"><input  style="" class="uk-checkbox delete-checkbox-color" type="checkbox" name="espremoverule[]" value="'.$id.'"></td>


<script type="text/javascript">
var sensorattr'.$id.' = $("[id=sensorattr'.$id.'] option").detach()
$("[id=Sensor'.$id.']").change(function() {
  var val = $(this).val()
  $("[id=sensorattr'.$id.'] option").detach()
  sensorattr'.$id.'.filter("." + val).clone().appendTo("[id=sensorattr'.$id.']")
}).change()
</script>


<script>
var visa'.$id.'="3";//visa'.$id.' is selected by default 
$("select[id=test'.$id.']").change(function () {
    document.getElementById(\'hidden_div'.$id.'\').style.visibility = this.value==visa'.$id.' ? \'visible\' : \'hidden\';
})
</script>

</tr>';

};

?>

</table>
</div>


<div style="display:inline-block;">
    <div class="livedata2"></div>
</div>
</div>




<!-- //#HERE -->
</div>
<br>
<div class="uk-card uk-card-default uk-card-body">

<!-- ---------------------------- -->



<div style="margin:0px;">
    <div style="display: inline-table;">
        <h3>Initiator</h3>
    </div>
    <div style="display: inline-table; float:right;">
        <div class="uk-button uk-button-default save-button" onclick="window.location.href ='addactionrule.php'">Add an Action rule</div>
    </div>
</div>


<table class="uk-table  " id="tableToModify" >
    <thead>        
        <th >Rule Description</th>
        <th>If Rule</th>    
        <th style="max-width:40px !important;">Is</th>
        <th>Then Set</th>
        <th style="max-width:20px;">To This % or #</th>
        <th>RULE ACTIVE</th>
        <th style="width:40px !important;">ALERT</th>
        
        <th style="max-width:0px !important;text-align: center;">MAX ALERT</th>
        <th style="min-width:20px;max-width:20px;">Alerts Sent</th>
        <th>DEL</th>
                
    </thead>
     <tbody>   

<?php

    $iffy2_id_array = array();
    $validator_id_array = array();
    $pca9685_id_array = array();
    $gpio_id_array = array();
    
            $stmt = $db->query("SELECT id FROM pca9685  WHERE node='$thisnode' ORDER BY channel;");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = ("pca9685,".$row['id']."");
                array_push($pca9685_id_array, $id);
            };

            $stmt = $db->query("SELECT id FROM gpio  WHERE node='$thisnode' ORDER BY `number`;");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = ("gpio,".$row['id']."");
                array_push($gpio_id_array, $id);
            };

            $stmt = $db->query("SELECT id FROM iffy2  WHERE node='$thisnode';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = $row['id'];
                array_push($iffy2_id_array, $id);
            };

            $stmt = $db->query("SELECT id FROM validator  WHERE node='$thisnode';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = $row['id'];
                array_push($validator_id_array, $id);
            };


    $stmt = $db->query("SELECT * FROM iffy3  WHERE node='$thisnode';");
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $description=$row['description'];
        $id = $row['id'];
        $alert = $row['alert'];
        
        $alertcan=$row['alertcan'];
        if ($alert==1) {$alertresult="checked";} else {$alertresult="";};
        
        $iffy2_id = $row['iffy2_id'];
        $validator_id = $row['validator_id'];
        $fact = $row['fact'];
            if ($fact==0) {$isTrue=""; $isFalse="selected";} else {$isTrue="selected"; $isFalse="";};
        $enable = $row['enable'];
            if ($enable==0) {$isTrueEnable=""; $isFalseEnable="selected";} else {$isTrueEnable="selected"; $isFalseEnable="";};
        $object = $row['object'];        
        if ($object=="gpio,0") {print "<div class='uk-alert-danger' uk-alert align='center'>
                        <a class='uk-alert-close' uk-close></a>
                        <p>An Initator below needs to be set in the \"THEN SET\" before activing rule.</p>
                    </div>";};        
        $objectvalue = $row['objectvalue'];
        
        print '<tr id="rowToClone">';
        print '            
            <td><input style="" class="uk-input" name="ifttt_3_description[]" value="'.$description.'"></td>
            <td style="width:70px;"><input name="ifttt3id[]" value="'.$id.'" hidden>

            <select style="max-width:70px;" class="uk-select" name="ifttt_3_rule_id[]">';
            
                foreach($iffy2_id_array as $key => $value) {
                    if ($iffy2_id == $value){$selected = "selected";}else{$selected="";};                    
                        print '<option value="'.$value.',pi" '.$selected.'>'.$value.'</option>';
                };
                foreach($validator_id_array as $key => $value) {
                    if ($validator_id == $value){$selected = "selected";}else{$selected="";};                    
                        print '<option value="'.$value.',esp" '.$selected.'>ESP('.$value.')</option>';
                };
             
            print '</select></td>       
            <td>            
            <select class="uk-select" name="ifttt_3_fact[]">
                <option value="0" '.$isFalse.'>False</option>
                <option value="1" '.$isTrue.'>True</option>
            </select>
                
                
            </td>
            <td>
            <select class="uk-select" name="ifttt_3_iffy2_id[]" id="type'.$id.'">';
            
                foreach($pca9685_id_array as $key => $value) {
                    $x = explode(",",$value);
                    print $x[1];
                    $stmt3 = $db->query("SELECT * from pca9685 WHERE id='$x[1]';");
                                while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
                                    $channelNumber=$row3['channel'];
                                    $pwmDescription=$row3['description'];
                            };
                    if ($object == $value){$selected = "selected";}else{$selected="";};                    
                        print '<option value="'.$value.'" '.$selected.'>PWM:'.$channelNumber.' ('.$pwmDescription.')</option>';
                };
                
                foreach($gpio_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    if ($object == $value){$selected = "selected";}else{$selected="";};
                            $stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    $gpioNumber=$row2['number'];
                                    $gpioDescription=$row2['description'];
                            };               
                print '<option value="'.$value.'" '.$selected.'>GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
                        };
                
                foreach($esp_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    if ($object == $value){$selected = "selected";}else{$selected="";};
                        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $espNumber=$row2['number'];
                                        $espDescription=$row2['description'];                                    
                                };               
                    
                    
                    
                        };
                        print '<option value="'.$value.',0" '.$selected.'>ESP: '.$espNumber.' ('.$espDescription.')</option>';
                    
                    if ($object == "telegram,0"){$selected = "selected";}else{$selected="";};print '<option value="telegram,0" '.$selected.'>Telegram</option>';

                    if ($object=="gpio,0") {$add="<option value='gpio,0' style='display:none;' selected>Please Set</option>";print $add;}; 
                    
            print '</select></td>            
            <td>
                
';

if ($objectvalue==0) {$offcheck="checked";} else {$offcheck="";}
if ($objectvalue==1) {$oncheck="checked";}else {$oncheck="";}
if ($objectvalue>1) {$pwmcheck="checked";}else {$pwmcheck="";}

$instances="";
$stmtI = $db->query("SELECT iffy3_id,COUNT(iffy3_id) AS instances FROM messages WHERE node='$thisnode' AND iffy3_id=".$id." AND timesent >= CURDATE() GROUP BY iffy3_id");
    while($rowI = $stmtI->fetch(PDO::FETCH_ASSOC)) {
        $instances = $rowI['instances'];        
};

// print $value;
print'        
<div id="size'.$id.'" style="padding:0px;margin:0px;">
Select
</div>


<script>

$(document).ready(function() {
    $("#type'.$id.'").change(function() {
        var val = $(this).val();
        if (val.includes("pca9685")) {
            $("#size'.$id.'").html("<input value =\''.$objectvalue.'\' class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'PWM\' min=\'0\' max=\'100\' type=\'number\' >");
        } else if (val.includes("gpio")) {
            $("#size'.$id.'").html("<input value =\''.$objectvalue.'\'  class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'GPIO\' placeholder=\'0 or 1\' min=\'0\' max=\'1\' type=\'number\' >");
        } else if (val.includes("esp")) {
            $("#size'.$id.'").html("<input value =\''.$objectvalue.'\' class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'ESP\' placeholder=\'0 or 1\' min=\'0\' max=\'1\' type=\'number\' >");

        }
          else if (val.includes("telegram")) {
            $("#size'.$id.'").html("<input value=\'0\'  name=\'ifttt_3_objectvalue[]\' hidden><div style=\'text-align:center;padding-top:7px;\'>Message Only</div>");

        }
      
    });


});

function zooloo'.$id.'() {    
        var val = document.getElementById("type'.$id.'").value;
        if (val.includes("pca9685")) {
            $("#size'.$id.'").html("<input value =\''.$objectvalue.'\' class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'PWM\' min=\'0\' max=\'100\' type=\'number\' >");
        } else if (val.includes("gpio")) {
             $("#size'.$id.'").html("<input value =\''.$objectvalue.'\'  class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'GPIO\' placeholder=\'0 or 1\' min=\'0\' max=\'1\' type=\'number\' >");
        } else if (val.includes("esp")) {
            $("#size'.$id.'").html("<input value =\''.$objectvalue.'\' class=\'uk-input\' name=\'ifttt_3_objectvalue[]\' placeholder=\'ESP\' placeholder=\'0 or 1\' min=\'0\' max=\'1\' type=\'number\' >");

        }
         else if (val.includes("telegram")) {
            $("#size'.$id.'").html("<input value=\'0\'  name=\'ifttt_3_objectvalue[]\' hidden><div style=\'text-align:center;padding-top:7px;\'>Message Only</div>");

        }
    };
zooloo'.$id.'();



</script>



               </td>
            <td>
            <select  id="ID'.$id.'" name="ifttt_3_enable[]'.$id.'" class="uk-select">
                <option value="1" '.$isTrueEnable.'>ON</option>
                <option value="0" '.$isFalseEnable.'>OFF</option>
            </select>
            </td>
            
            <td style=""><input  class="uk-checkbox " type="checkbox" name="alert[]" value="'.$id.'" '.$alertresult.'>    </td>
    
    
    
    

    <td style="width:100px !important;"><input class="uk-input " type="number" required name="alertcan[]" value="'.$alertcan.'"></td>
    <td style="text-align:center;padding-top:7px;">'.$instances.'</td>

<td style="width:20px !important;"><input  style="" class="uk-checkbox delete-checkbox-color" type="checkbox" name="ifttt3remove[]" value="'.$id.','.$thisnode.'"></td>

            ';
        print '</tr>';
        $disable_b="";

       print ' <script type="text/javascript">

function f_color'.$id.'(){
if (document.getElementById(\'ID'.$id.'\').value == \'1\') {
document.getElementById(\'ID'.$id.'\').style.color = "white";
document.getElementById(\'ID'.$id.'\').style.background="lightgreen";}
else if (document.getElementById(\'ID'.$id.'\').value == \'0\') {
document.getElementById(\'ID'.$id.'\').style.color = "black";
document.getElementById(\'ID'.$id.'\').style.background="lightgrey";}
}
document.getElementById(\'ID'.$id.'\').onchange= f_color'.$id.'();

</script>';

    };
?>

   </tbody>
</table>
<button class="uk-button uk-button-default save-button" type="submit" id="submit" style="">Save</button>
</form>

</div>
<script type="text/javascript">
function myFunction(){
    $('#submit').click();
};

</script>
<style type="text/css">
    .low-risk{
    background: green;
}

.medium-risk{
    background: orange;
}

.high-risk{
    background: red;
}
</style>


<form action="submit.php" method="POST" id="addespruleform">
<input name="option" value="espunitsaddrule" hidden>
<input name="thisnode" value="<?php print $thisnode;?>" hidden>

</form>






<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 1000 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('rulesiffy3.php');
    $(".livedata1");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata1").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>

<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 1200 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('rulesiffy3_time.php');
    $(".livedata3");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata3").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>

<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 1100 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('rulesvalidator.php');
    $(".livedata2");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata2").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>

</info>