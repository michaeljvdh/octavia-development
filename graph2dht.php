<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
 

<?php
$thetable = $_GET['thetable'];
$theattr = $_GET['theattr'];
$limit = $_GET['limit'];

include "header.php";
include "connection.php";

$trendlines = '
trendlines: {
    0: {
      type: \'linear\',
      color: \'green\',
      lineWidth: 3,
      opacity: 0.3,
      showR2: true,
      visibleInLegend: true
    }},
';

$ds18b20_collection = array();
$ds18b20_labels = array();

$stmt = $db->query("SELECT * FROM $thetable;");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    array_push($ds18b20_collection, $row['id']);
    array_push($ds18b20_labels, $row['description']);
  };

$unique=mt_rand();;
foreach ($ds18b20_collection as $key => $value) {
  print'
  <div class="uk-card uk-card-small uk-card-default" style="max-width:600px;margin-bottom:5px;">
 <div style="max-width:600px;">
    <div id="chart_div'.$value.$unique.'"></div>    
  </div>
  <div style="max-width:600px;">
    <div id="chart_div_h'.$value.$unique.'"></div>    
  </div>
</div>
  ';

$ds18b20_temperature_array = array();
$count = 0;
$stmt = $db->query("SELECT * FROM log WHERE `table`='$thetable' and attr='temperature' and table_id='$value' ORDER BY id DESC LIMIT $limit;");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    $valued = $row['value'];
    $time = $row['stamp'];
    $attrname = $row['attr'];

    $time = strtotime($time);
    $time = date("H:i:s",$time);


    $valued = "[".$count.",".$valued."]";

    array_push($ds18b20_temperature_array, $valued);
    $count = $count + 1;
  };

  $ds18b20_humidity_array = array();
$count = 0;
$stmt = $db->query("SELECT * FROM log WHERE `table`='$thetable' and attr='humidity' and table_id='$value' ORDER BY id DESC LIMIT $limit;");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    $valued = $row['value'];
    $time = $row['stamp'];
    $attrname = $row['attr'];

    $time = strtotime($time);
    $time = date("H:i:s",$time);


    $valued = "[".$count.",".$valued."]";

    array_push($ds18b20_humidity_array, $valued);
    $count = $count + 1;
  };

print_r($ds18b20_temperature_array);
print '
<script>
google.charts.load(\'current\', {packages: [\'corechart\', \'line\']});
google.charts.setOnLoadCallback(drawLineColors);

function drawLineColors() {
      var data'.$value.' = new google.visualization.DataTable();
      data'.$value.'.addColumn(\'number\', \'X\');
      data'.$value.'.addColumn(\'number\', \'123\');      
      data'.$value.'.addRows([
   ';   
 print implode($ds18b20_temperature_array,",");
     print ' 

      ]);

      var options = {
        '.$trendlines.'
        // hAxis: {
        //   title: \'Time\'
        // },
        // vAxis: {
        //   title: \'Popularity\'
        // },
        title:\''.$ds18b20_labels[$key].' : Temperature\',
        colors: [\'#a52714\', \'#097138\'],
        curveType:\'function\',
        legend: \'none\',
      };

      var chart'.$value.' = new google.visualization.LineChart(document.getElementById(\'chart_div'.$value.$unique.'\'));
      chart'.$value.'.draw(data'.$value.', options);
    }
  </script>

<script>
google.charts.load(\'current\', {packages: [\'corechart\', \'line\']});
google.charts.setOnLoadCallback(drawLineColors);

function drawLineColors() {
      var data_h'.$value.' = new google.visualization.DataTable();
      data_h'.$value.'.addColumn(\'number\', \'X\');
      data_h'.$value.'.addColumn(\'number\', \'123\');      
      data_h'.$value.'.addRows([
   ';   
 print implode($ds18b20_humidity_array,",");
     print ' 

      ]);

      var options = {
        '.$trendlines.'
        // hAxis: {
        //   title: \'Time\'
        // },
        // vAxis: {
        //   title: \'Popularity\'
        // },
        title:\''.$ds18b20_labels[$key].' : Humidity\',
        colors: [\'#a52714\', \'#097138\'],
        curveType:\'function\',
        legend: \'none\',
      };

      var chart_h'.$value.' = new google.visualization.LineChart(document.getElementById(\'chart_div_h'.$value.$unique.'\'));
      chart_h'.$value.'.draw(data_h'.$value.', options);
    }
  </script>';











};
  






  ?>