
<?php
$noderequest = $_GET['node'];
include "connection.php";
include "header.php";
include "nav.php";

$addpitables = array();
array_push($addpitables, 'masterrelay');
$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$tablename = $row['Tables_in_octavia'];
		array_push($addpitables, $tablename);
	;};

$onchange = "";
$noderequest_strip = str_replace("node_", "", $noderequest);
$noderequest_strip = str_replace("_", " ", $noderequest_strip);
?>
<style type="text/css">
	.stylemyinput_checkbox {
	min-height: 30px;
	min-width: 30px;		
	}
	th {
		text-align: center;
	}
 :focus {outline: none !important;}
</style>

<div class="uk-container">
	<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Add DHT11/22 Sensor (Raspberry Pi)</h3>
<!-- Accordian open -->

<!-- Data -->

	<form class="" name = "addtodht" action="submit.php" method="POST" style="margin:10px;padding-bottom: 10px;">
		<table class="uk-table " id="tbl_posts">
				<input name="option" value = "addtodht" hidden>
				<input name="fromnode" value="<?php print $thisnode; ?>" hidden>
				<tr>
					<thead>
							<th>Description</th>							
							<th>Gpio</th>
							<th></th>
					</thead>
				</tr>
			<tbody id="tbl_posts_body">
			<tr id="rec-1">
<!-- REPLICATION PORTION -->
				<td><input class="uk-input" name="dhtdescription[]" required></td>
				<td><input class="uk-input" name="dhtgpio[]" required></td>
					
<!-- REPLICATION PORTION -->
					<td  style="text-align: center;"><a class="add-record" data-added="0"><div style="position:relative;top:-5px;padding-bottom:0px;font-size:2em;width:100%;color: lightgreen;">+</div></a></td>

          		</tr>
			</tbody>
			
			</table>
			<input type="text" value="master" name="fromurl" hidden>
			<button class="<?php print $theme;?> uk-button uk-button-default save-button" type="submit">Add</button>
		</form>


</div>
       
       <br>

<div style="display:none;">
    <table id="sample_table">
      <tr id="">
<!-- REPLICATION PORTION -->
				<td><input class="uk-input" name="dhtdescription[]" required></td>
				<td><input class="uk-input" name="dhtgpio[]" required></td>
<!-- REPLICATION PORTION -->
				<td  style="text-align: center;"><a class="btn btn-xs delete-record" data-id="0"><i class="uk-icon-link" uk-icon="trash"  style="color: red;"></a></td>

     </tr>     
   </table>
 </div>

<div class="uk-animation" tabindex="0"><!-- Animation Start -->
    <div class="uk-animation-scale-up uk-transform-origin-top-center"><!-- Animation Start -->

<script type="text/javascript">
	jQuery(document).delegate('a.add-record', 'click', function(e) {
		e.preventDefault();    
		var content = jQuery('#sample_table tr'),
		size = jQuery('#tbl_posts >tbody >tr').length + 1,
		element = null,    
		element = content.clone();
		element.attr('id', 'rec-'+size);
		element.find('.delete-record').attr('data-id', size);
		element.appendTo('#tbl_posts_body');
		element.find('.sn').html(size);
	});

	jQuery(document).delegate('a.delete-record', 'click', function(e) {
		e.preventDefault();    
		// var didConfirm = confirm("Are you sure You want to delete");
		// if (didConfirm == true) {
			if (1 == 1) {
		var id = jQuery(this).attr('data-id');
		var targetDiv = jQuery(this).attr('targetDiv');
		jQuery('#rec-' + id).remove();
		
		//regnerate index number on table
		$('#tbl_posts_body tr').each(function(index) {
		//alert(index);
		$(this).find('span.sn').html(index+1);
		});
		return true;
	} else {
		return false;
	}
	});
</script>
</div>
</div>