<?php
include "connection.php";
include "header.php";
include "nav.php";

// $myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
// 		$thisnode = fgets($myfile);
// 		$thisnode = str_replace('`', '', $thisnode);
// 		fclose($myfile);
// 		$thisnode = trim($thisnode);
//         $value=$thisnode;





?>

<!-- HELP -->
<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Adding a GPIO on the Raspberry Pi</h2>
        <h4>Interval (Sec)</h4>
        <p>This will log entries based on seconds, for example, 300 seconds would mean a value will be written to the log every 5 min.</p>
        <h4>Records</h4>
        <p>This allows you to specify how many revolving records you would like to keep.  10 for example, at every tenth interval 50% of records will be discarded and 50% replenished, this allows a small history to remain so your able to see the comparative as it goes along.</p>
        <h4>Display</h4>
        <p>Literally how many entries to show on the graph.  You might capture 1000 records, but displaying that is illogical, hence you can limit the display to x records.  For example, if you captured every half hour, setting this value to 10 makes sense.  That would allow you to see 5 hours of data.</p>        
    </div>
</div>
<!-- HELP -->


<form action="submit.php" method="POST">
    <input name="option" value="logging" hidden>
    <input name="thisnode" value="<?php print $thisnode;?>" hidden>
<input id="" name="frompage" value="configdht1122.php" hidden >
<div class="uk-container">
	<div class="uk-card uk-card-default uk-card-body">
    <div style="display: inline-table;"><h3 class="uk-card-title">Logging (On *This Raspberry Pi)</h3></div>
    <div style="display: inline-table;float: right;"><a class="" href="#modal-container" uk-toggle><span uk-icon="icon: question;"></span></a></div>
<?php

$stmt = $db->query("select * from config where description='log_display_length';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  $display = $row['set1'];
  // $display = intval($display);
  };


print '<div class="ukTableCard">';

print '
<div class="">
<div class="container" >


<table class="uk-table">
<thead>
<th>Desciption</th><th>Interval (Sec)</th><th>Records</th><th>Display</th>
</thead>';
$stmt4 = $db->query("SELECT * FROM config WHERE description='log' AND set1='global';");

while($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
        $dhtid = $row4['id'];
        $dhtdescription = $row4['set1'];
        $interval = $row4['set2'];
        $records = $row4['set3'];        
        $display = $row4['set4'];        
        
print '
<input name="logid[]" value="'.$dhtid.'" hidden>
<tr><td><input class="uk-input" value="'.$dhtdescription.'" disabled></td>
<td><input class="uk-input" name="loginterval[]" min="10" value="'.$interval.'"  type="number" required></td>
<td><input class="uk-input" name="logrecords[]" value="'.$records.'" type="number" required min="1"></td>
<td><input class="uk-input" name="display[]" value="'.$display.'" type="number" required min="1"></td>

</tr>
';

};
print '
</table>
<button class= "uk-button uk-button-default save-button" type="submit">UPDATE</button>
</div>
</div>
</div>';
// ADD DHT TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// DHT CODE BLOCK


print '

';

$stmt = $db->query("select count(id) as records from log where node='$thisnode';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {  
  $records = $row['records'];
  };






?>

</form>
</div>
</div>
<div class="uk-container">
<form action="submit.php" method="POST">    
<input name="option" value="logdump" hidden>
<input name="thisnode" value="<?php print $thisnode;?>" hidden>
<button type="submit" class="uk-button uk-button-default save-button">Clear Log</button>
<?php print $records."&nbsp; Records Currently";?>
</form>
</div>