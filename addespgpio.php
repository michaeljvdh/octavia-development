<?php

include "connection.php";
include "header.php";
include "nav.php";

$onchange = "";

?>

<!-- HELP -->
<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Adding a GPIO on the Raspberry Pi</h2>
        <h4>Description</h4>
        <p>A Description for the pin.  This can either be it's true name, or a friendly name.  There is no consequence.</p>
        <h4>GPIO</h4>
        <p>The ESP GPIO number, note that this project is run in board mode.  So it's the literal pin count and NOT the normal ESP GPIO Number. Below is an example of how to reference the numbers.</p>
        <p><img src="/assets/help/esp8266.png" style="max-width: 600px; "></p>
        <h4>Polarity</h4>
        <p>In some cases with some devices, on and off needs reversing.  As an example, low level trigger relays, need their polarity to be 0, where high level needs to be 1.  1 is default and high-level relays are recommended and considered to be the norm.</p>
        <p>What's the difference?, with low level relays, when power is lost to the board .. they will flip to the on position ... not really desired if conencted to a water pump.</p>
        <p>High-level relays if power is lost .. it will NOT switch on as a result .. it OFF is OFF period.</p>
        <p>There is some application for low-level relays, but we dont recommend them unless you are puposefully using them, and once again ... that is the reason we provide a polarity reversale i.e. 0</p>
        <h4>Code for a WEMOS ESP8266 Mini</h4>
        <p>Refer to the installation instructions, for more information on uploading the .ino file to your ESP8266.  We are not limited to this single configuration.  In fact we are creating more.</p>
    </div>
</div>
<!-- HELP -->


<style type="text/css">
    .stylemyinput_checkbox {
    min-height: 30px;
    min-width: 30px;        
    }
    th {
        text-align: center;
    }
 :focus {outline: none !important;}
</style>

<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body">
    <div>
        <div style="display: inline-table;"><h3 class="uk-card-title">Add a GPIO to ESP8266</h3></div>
        <div style="display: inline-table;float: right;"><a class="" href="#modal-container" uk-toggle><span uk-icon="icon: question;"></span></a></div>
    </div>
<!-- Accordian open -->

<!-- Data -->
<div class="<?php print $theme;?>"  >
    <form class="" name = "" action="submit.php" method="POST" style="margin:10px;padding-bottom: 10px;">
        <table class="uk-table " id="tbl_posts">
                <input name="option" value = "addespgpio" hidden>
                <input name="thisnode" value="<?php print $thisnode; ?>" hidden>
                <tr>
                    <thead>
                            <th>Description</th>                            
                            <th>IP Address</th>
                            <th>Gpio</th>
                            <th>POLARITY</th>
                            <th></th>
                    </thead>
                </tr>
            <tbody id="tbl_posts_body">
            <tr id="rec-1">
<!-- REPLICATION PORTION -->                
<td><input type="text" class="uk-input" name="gpioDescription[]" required></td>
<td><input type="text" class="uk-input" name="ipaddr[]" required></td>
<td><input type="number" min="1" max="8" class="uk-input" name="gpioNumber[]" required></td>
<td><input type="number" min="0" max="1"class="uk-input" name="gpioPolarity[]" value="1"></td>   
<!-- REPLICATION PORTION -->
                    <td  style="text-align: center;"><a class="add-record" data-added="0"><i class="uk-icon-link" uk-icon="plus" style="color: lightgreen;"></a></td>

                </tr>
            </tbody>
            
            </table>
            <input type="text" value="master" name="fromurl" hidden>
            <button class="<?php print $theme;?> uk-button uk-button-default save-button" type="submit">Add</button>
        </form>


</div>
       
       <br>

<div style="display:none;">
    <table id="sample_table">
      <tr id="">
<!-- REPLICATION PORTION -->
<td><input type="text" class="uk-input" name="gpioDescription[]" required></td>
<td><input type="text" class="uk-input" name="ipaddr[]" required></td>
<td><input type="number" min="1" max="8" class="uk-input" name="gpioNumber[]" required></td>
<td><input type="number" min="0" max="1"class="uk-input" name="gpioPolarity[]" value="1"></td>   
<!-- REPLICATION PORTION -->
                <td  style="text-align: center;"><a class="btn btn-xs delete-record" data-id="0"><i class="uk-icon-link" uk-icon="trash"  style="color: red;"></a></td>

     </tr>     
   </table>
 </div>

</div>
<script type="text/javascript">
    jQuery(document).delegate('a.add-record', 'click', function(e) {
        e.preventDefault();    
        var content = jQuery('#sample_table tr'),
        size = jQuery('#tbl_posts >tbody >tr').length + 1,
        element = null,    
        element = content.clone();
        element.attr('id', 'rec-'+size);
        element.find('.delete-record').attr('data-id', size);
        element.appendTo('#tbl_posts_body');
        element.find('.sn').html(size);
    });

    jQuery(document).delegate('a.delete-record', 'click', function(e) {
        e.preventDefault();    
        // var didConfirm = confirm("Are you sure You want to delete");
        // if (didConfirm == true) {
            if (1 == 1) {
        var id = jQuery(this).attr('data-id');
        var targetDiv = jQuery(this).attr('targetDiv');
        jQuery('#rec-' + id).remove();
        
        //regnerate index number on table
        $('#tbl_posts_body tr').each(function(index) {
        //alert(index);
        $(this).find('span.sn').html(index+1);
        });
        return true;
    } else {
        return false;
    }
    });
</script>
