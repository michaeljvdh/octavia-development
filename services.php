

    <?php
        
        include "connection.php";
        include "header.php";
        include "nav.php";

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
		$thislocalnode = fgets($myfile);		
		$thislocalnode = str_replace('`', '', $thislocalnode);
		fclose($myfile);		
		$thislocalnode = trim($thislocalnode);		
		// print $thislocalnode;
        // $value=$thislocalnode;
        $nodelabel = "node_Raspberry_Pi_2"        ;
        $level1 = str_replace("node_", "", $nodelabel);
        // print "<br>".$level1;
        $level2 = str_replace("_", " ", $level1);
        $nodelabel = $level2;

        // print $nodelabel;

        if ($thislocalnode=="masterrelay") {$nodelabel="Master PI";};

    ?>
</head>
<body>
<div class="uk-container">
	<div class="uk-card uk-card-default uk-card-body ">
    	<h5 class="uk-card-title">SERVICES</h5>
  
  		<?php
  			print "The following services are indicated purely for the node named <strong style='font-size:1.2em;'>".$nodelabel."</strong>. These services are designed to interact with locally connected devices and sensors.  To validate services on other nodes, check the servics page from the respective node web URL, and navigate to the services page. The <font style='color:red; font-weight:bold;'>'View Selector at the top right does NOT impact this page'.</font>";
  		?>


<!-- <form> -->
<br>

		
<!-- </form> -->

  <div class="uk-card uk-card-default " style="background-color: white; border-radius: 5px; padding: 20px;margin-top:20px;">
  <h4 class="headinds">SERVICE STATUSES</h4>
  <?php
  
    exec('systemctl list-units --type=service | grep Octavia ',$output);
    // print '<pre>';
    // print_r($output);
    // print '</pre>';
    print '<table class="table" style="margin-left:30px;">';
    // print_r($output);
    foreach ($output as $key => $value) {
      print '<strong>';
      $value = str_replace("running", "<td><font style='padding-left: 5px; padding-right: 5px;color:white; background: green;'>ACTIVE</font></td>", $value);
      $value = str_replace("failed", "<td><font style='padding-left: 5px; padding-right: 5px;color:white; background: red;'>FAILED</font></td>", $value);
      
      // $value = str_replace(" ATO", "", $value);
      // $value = str_replace(" Doser", "", $value);
      // $value = str_replace(" Feeder", "", $value);
      // $value = str_replace(" Manual Relay", "", $value);
      // $value = str_replace(" SChedule Check", "", $value);
      // $value = str_replace(" Therm Check", "", $value);
      // $value = str_replace(" Wave-A", "", $value);
      // $value = str_replace(" Wave-B", "", $value);


      print '<td>'.$value . '</td></tr>';
      print '</strong>';
      # code...
    }
    print '</table>';

  ?>
</div>
  <p><button class="uk-button uk-button-default save-button" onClick="history.go(0)" VALUE="Refresh Now">REFRESH</button></p>
  <br>
  <hr>
<h3 class="headinds">MANAGING SERVICES</h3>
<ol>
<li>
  Simply break to the command line on the raspberry pi and type in <strong>sudo systemctl stop xxxx</strong> where xxxx = schedulecheck.service ( or which ever service you wish to stop.)
</li>
<li>
  to Start the service again do the opposite. <strong>sudo systemctl start xxxx</strong>
</li>
<li>
  And if you wish to validate services stopped or started at the command prompt type in <strong>sudo systemctl list-units --type=service | grep Octavia</strong>
</li>
</ol>
<br>
</div>
</div></body>